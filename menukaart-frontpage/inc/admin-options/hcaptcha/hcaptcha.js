// Callback login form
function onSubmitLogin(token) {
    document.getElementById("loginform").submit();
}

// Callback register form
function onSubmitRegister(token) {
    document.getElementById("registerform").submit();
}

// Callback password form
function onSubmitPassword(token) {
    document.getElementById("lostpasswordform").submit();
}

// Load jQuery
jQuery(document).ready(function ($) {
    // Prevent form submissions
    $('#loginform, #registerform, #lostpasswordform').submit(function (e) {
        e.preventDefault()

        // Execute hCaptcha
        hcaptcha.execute()
    })
})