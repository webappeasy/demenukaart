<?php
 
function custom_theme_assets() {

    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'bootstrap-script', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js' , NULL , false , true );
    wp_enqueue_script( 'bootstrap-script2', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js' , NULL , false , true );
    wp_enqueue_script( 'lightbox-script', 'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js' , NULL , false , true );
    wp_enqueue_script( 'filter-script' , get_template_directory_uri() . '/js/menu.js' , NULL , false , true );
    wp_enqueue_script( 'bs-init-script' , get_template_directory_uri() . '/assets/js/bs-init.js' , NULL , false , true );
    wp_enqueue_script( 'main-script' , get_template_directory_uri() . '/assets/js/main.js' , NULL , false , true );
    wp_enqueue_script( 'storage-script' , get_template_directory_uri() . '/js/storage.js' , NULL , false , true );
    wp_enqueue_style( 'bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', array(), null );
    wp_enqueue_style( 'featuresboxed-style' , get_template_directory_uri() . '/assets/css/Features-Boxed.css' );
    wp_enqueue_style( 'headerdark-style' , get_template_directory_uri() . '/assets/css/Header-Dark.css' );
    wp_enqueue_style( 'navigationclean-style' , get_template_directory_uri() . '/assets/css/Navigation-Clean.css' );
    wp_enqueue_style( 'navigationbutton-style' , get_template_directory_uri() . '/assets/css/Navigation-with-Button.css' );
    wp_enqueue_style( 'navigationsearch-style' , get_template_directory_uri() . '/assets/css/Navigation-with-Search.css' );
    wp_enqueue_style( 'featuresboxed-style' , get_template_directory_uri() . '/assets/css/Features-Boxed.css' );
    wp_enqueue_style( 'teamboxed-style' , get_template_directory_uri() . '/assets/css/Team-Boxed.css' );
    wp_enqueue_style( 'style', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'custom_theme_assets' );

function create_posttype() {
 
    register_post_type( 'menukaart',
        array(
            'labels' => array(
                'name'          => __( 'Menukaart' ), 
                'singular_name' => __( 'Menukaart' ),
                'add_new'       => __( 'Menukaart aanmaken', 'textdomain' ),
                'add_new_item'  => __( 'Nieuwe menukaart maken', 'textdomain' ),
                'new_item'      => __( 'Nieuwe menukaart', 'textdomain' ),
                'edit_item'     => __( 'Verander menukaart', 'textdomain' ),
                'view_item'     => __( 'Bekijk menukaart', 'textdomain' ),
                'all_items'     => __( 'Alle menukaarten', 'textdomain' ),
                'search_items'  => __( 'Zoek menukaarten', 'textdomain' ),
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'has_archive' => true,
            'menu_icon' => 'dashicons-text-page',
        )
    );
}
// Hooking up our function the theme
add_action( 'init', 'create_posttype' );

// Convert category slug to lowercase and replace space with a dash
function formatUrl($str, $sep='-'){
    $res = strtolower($str);
    $res = preg_replace('/[^[:alnum:]]/', ' ', $res);
    $res = preg_replace('/[[:space:]]+/', $sep, $res);
    return trim($res, $sep);
}

// ACF custom styling for the back-end

function my_acf_admin_head() {
    ?>
    <style type="text/css">
    /*
    .acf-fields.-left>.acf-field>.acf-label {
        width: 11%;
    }*/
    .acf-fields.-left>.acf-field:before {
        width: 0%;
    }
    .acf-fields.-left>.acf-field>.acf-input {
        width: 100%;
    }

    .acf-input .acf-custom-category-name {
        padding-left: 1%!important;
    }
    .acf-custom-price-group-backend .acf-fields.-left>.acf-field>.acf-label {
        width: 20%;
    }
    .acf-custom-price-group-backend .acf-fields.-left>.acf-field:before {
        width: 20%;
    }
    .acf-custom-price-group-backend .acf-fields.-left>.acf-field>.acf-input {
        width: 80%;
    }
    .acf-custom-product-collapse {
        padding-left: 1%!important;
    }
    .acf-accordion-content.acf-input {
        width: 100%!important;
    }
        </style>
    <?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');

/* Add featured image support */
function add_featured_image_support_to_your_wordpress_theme() {
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'small-thumbnail', 100, 100, true );
}
 
add_action( 'after_setup_theme', 'add_featured_image_support_to_your_wordpress_theme' );

// Make user have only 1 column
function so_screen_layout_columns( $columns ) {
    $columns['menukaart'] = 1;
    return $columns;
}
add_filter( 'screen_layout_columns', 'so_screen_layout_columns' );

function so_screen_layout_post() {
    return 1;
}
add_filter('get_user_option_screen_layout_menukaart', function() {
    return 1;
});

// Customize dashboard for new users (hidding default meta boxes)
function remove_dashboard_widgets() {
    global $wp_meta_boxes;
 
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
 
}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

// Custom dashboard widget for new users
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

function my_custom_dashboard_widgets() {
global $wp_meta_boxes;
 
wp_add_dashboard_widget('custom_help_widget', 'Digitale menukaart', 'custom_dashboard_help');
}
 
function custom_dashboard_help() {
echo '<p>Welkom in de beheer omgeving van demenukaart.app.</p>';
echo '<p>Het platform om je menukaart te digitaliseren zodat de gasten deze via hun eigen smartphone kunnen bekijken.</p>';
echo '<p>Heb je help nodig? neem dan contact met ons op via <a href="tel:0031111407419">0111-407419</a>.</p>';
}

// Restrict media for only the users own media files
add_filter( 'ajax_query_attachments_args', 'wpb_show_current_user_attachments' );
 
function wpb_show_current_user_attachments( $query ) {
    $user_id = get_current_user_id();
    if ( $user_id && !current_user_can('activate_plugins') && !current_user_can('edit_others_posts
') ) {
        $query['author'] = $user_id;
    }
    return $query;
} 
// Get login global
require get_stylesheet_directory() . '/global.php';

// Thumbnails
add_theme_support('post-thumbnails');

// Custom Background
add_theme_support('custom-background');