<?php
get_header();
?>

<!-- Header  -->
<div class="features-boxed" style="background-color: rgba(0,0,0,0);">
    <div class="container pb-5">
        <div class="intro">
            <h2 class="text-center">Mogelijkheden</h2>
            <p class="text-center">Een digitale menukaart voor ieder restaurant.</p>
        </div>
    </div>
</div>
    <div class="cover-container bg-dark pt-5 pb-5">
        <div class="container bg-dark rounded">
            <div class="row">
                <div class="col-6 pt-5"> 
                <iframe src="https://www.demenukaart.app/brasserie-de-kaoie" width="100%" height="800px" style="border:1px solid white;"></iframe>
                </div>
                <div class="col-6">
                    <section>
                        <div class="container pb-1 pt-4 text-center features-boxed" style="background-color: rgba(0,0,0,0);";>
                            <div class="p-1">
                                <h3 class="text-light">Categorieën</h3>
                                <p class="lead mx-auto mb-5 text-muted">Binnen je menukaart kan je zelf categorieën aanmaken. Deze verschijnen in een navigatie en sorteren je dranken en gerechten. Hierdoor kunnen je gasten alles gemakkelijk vinden.</p>
                            </div>
                            <div class="p-1">
                                <h3 class="text-light">Product mogelijkheden</h3>
                                <p class="lead mx-auto mb-5 text-muted">Bij het aanmaken van nieuwe producten heb je tal van opties. Er is dus altijd genoeg plek voor informatie, prijzen en een afbeelding bij je producten.</p>
                            </div>
                            <div class="p-1">
                                <h3 class="text-light">QR-code</h3>
                                <p class="lead mx-auto mb-5 text-muted">Bij iedere digitale menukaart leveren wij een persoonlijke QR-code. De gast kan met deze code gemakkelijk en snel bij je digitale menukaart komen.</p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<?php    
get_footer();
?>