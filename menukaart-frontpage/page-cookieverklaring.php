<?php
get_header();
?>

<!-- Header  -->
<div class="features-boxed" style="background-color: rgba(0,0,0,0);">
    <div class="container pb-5">
        <div class="intro">
            <h2 class="text-center">Cookieverklaring</h2>
            <p class="text-center">Voor informatie over prijzen neem contact met ons op.</p>
        </div>
    </div>
</div>
<!-- Features -->
<section class="bg-dark">
    <div class="container p-5 z-depth-1 unique-color-dark">
        <!--Section: Content-->
        <section class="text-center text-light">
        <h2 class="mb-4 pb-2">Een digitale menukaart</h2>
            <p>1. Het gebruik van cookies</p>
            <p>www.demenukaart.app maakt gebruik van cookies. Een cookie is een eenvoudig klein bestandje dat met pagina’s van deze website en/of Flash-applicaties wordt meegestuurd en door je browser op je harde schrijf van je computer, mobiele telefoon, smartwatch of tablet wordt opgeslagen. De daarin opgeslagen informatie kan bij een volgend bezoek weer naar onze servers teruggestuurd worden.
            Het gebruik van cookies is van groot belang voor het goed laten draaien van onze website, maar ook cookies waarvan je niet direct het effect ziet zijn zeer belangrijk. Dankzij de (anonieme) input van bezoekers kunnen we het gebruik van de website verbeteren en deze gebruiksvriendelijker maken.</p>
            <p>2. Toestemming voor het gebruik van cookies</p>
            <p>Voor het gebruik van bepaalde cookies is jouw toestemming vereist. Dit doen wij door middel van een zogenaamde cookiebanner.</p>
            <p>3. De gebruikte type cookies en hun doelstellingen</p>
            <p>Wij gebruiken de volgende type cookies:
            Functionele cookies: hiermee kunnen we de website beter laten functioneren en is het gebruikersvriendelijker voor de bezoeker.
            Analytische cookies: deze zorgen ervoor dat iedere keer wanneer je een website bezoekt er een anonieme cookie wordt gegenereerd. Deze cookies weten of je de site al eerder bezocht hebt of niet. Alleen bij het eerste bezoek, wordt er een cookie aangemaakt, bij volgende bezoeken wordt er gebruik gemaakt van de reeds bestaande cookie. Deze cookie dient enkel voor statistische doeleinden. Zo kunnen daarmee de volgende data verzameld worden:
            <ul class="text-left"><li>het aantal unieke bezoekers</li>
            <li>hoe vaak gebruikers de site bezoeken</li>
            <li>welke pagina’s gebruikers bekijken</li>
            <li>hoe lang gebruikers een bepaalde pagina bekijken</li>
            <li>bij welke pagina bezoekers de site verlaten</li>
            </ul></p>
            <p>4. Je rechten met betrekking tot je gegevens</p>
            Je hebt het recht op inzage, rectificatie, beperking en verwijdering van persoonsgegevens. Daarnaast heb je recht van bezwaar tegen verwerking van persoonsgegevens en recht op gegevensoverdraagbaarheid. Je kunt deze rechten uitoefenen door ons een mail te sturen via mail@demenukaart.app.
            Om misbruik te voorkomen kunnen wij je daarbij vragen om je adequaat te identificeren. Wanneer het gaat om inzage in persoonsgegevens gekoppeld aan een cookie, vragen we je een kopie van het cookie in kwestie mee te sturen. Je kunt deze terug vinden in de instellingen van je browser.
            <p>5. Cookies blokkeren en verwijderen</p>
            <p>Je kunt cookies ten alle tijden eenvoudig zelf blokkeren en verwijderen via je internetbrowser. Ook kun je je internetbrowser zodanig instellen dat je een bericht ontvangt als er een cookie wordt geplaatst. Je kunt ook aangeven dat bepaalde cookies niet geplaatst mogen worden. Bekijk voor deze mogelijkheid de helpfunctie van je browser. Als je de cookies in je browser verwijdert, kan dat gevolgen hebben voor het prettig gebruik van deze website.
            Wees er wel van bewust dat als je geen cookies wilt, wij niet meer kunnen garanderen dat onze website helemaal goed werkt. Het kan zijn dat enkele functies van de site verloren gaan of zelfs dat je de website helemaal niet meer kunt bezoeken.
            Hoe je je instellingen kunt aanpassen, verschilt per browser. Raadpleeg indien nodig de helpfunctie van jouw browser, of klik op één van de onderstaande weblinks om direct naar de handleiding van je browser te gaan.
            Firefox: https://support.mozilla.org/nl/kb/cookies-verwijderen-gegevens-wissen-websites- opgeslagen
            Google Chrome: https://support.google.com/chrome/answer/95647?co=GENIE.Platform=Desktop&hl=nl Internet Explorer: https://support.microsoft.com/nl-nl/kb/278835
            Safari: https://support.apple.com/kb/ph21411?locale=nl_NL</p>
            <p>6. Nieuwe ontwikkelingen en onvoorziene cookies</p>
            <p>De teksten van onze website kunnen op ieder moment worden aangepast door voortdurende ontwikkelingen. Dit geldt ook voor onze cookieverklaring. Neem deze verklaring daarom regelmatig door om op de hoogte te blijven van eventuele wijzigingen.
            In blogartikelen kan gebruik worden gemaakt van content die op andere sites wordt gehost en op demenukaart.app wordt ontsloten door middel van bepaalde codes (embedded content). Denk hierbij aan bijvoorbeeld YouTube video’s. Deze codes maken vaak gebruik van cookies. Wij hebben echter geen controle op wat deze derde partijen met hun cookies doen.
            Het kan ook voorkomen dat via onze websites cookies worden geplaatst door anderen, waarvan wijzelf niet altijd op de hoogte zijn. Kom je op onze website onvoorziene cookies tegen die je niet kunt terugvinden in ons overzicht? Laat het ons weten via mail@demenukaart.app. Je kan ook rechtstreeks contact opnemen met de derde partij en vraag welke cookies ze plaatsten, wat de reden daarvoor is, wat de levensduur van de cookie is en op welke manier ze je privacy gewaarborgd hebben.
            </p>
            <p>7. Slotopmerkingen</p>
            <p>Wij zullen deze verklaringen af en toe aan moeten passen, bijvoorbeeld wanneer onze we onze website aanpassen of de regels rondom cookies wijzigen. Je kunt deze webpagina raadplegen voor de laatste versie.
            Mocht je nog vragen en/of opmerkingen hebben neem dan contact op met mail@demenukaart.app.
            Deze versie is opgesteld in mei 2020.</p>
    </div>
</section>

<?php
get_footer();
?>