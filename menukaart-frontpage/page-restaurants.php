<?php
get_header();
?>
<!-- Restaurants overview -->
<div class="features-boxed" style="background-color: rgba(0,0,0,0);">
    <div class="container pb-5">
        <div class="intro">
            <h2 class="text-center">Aangesloten restaurants</h2>
            <p class="text-center mb-5">Vind hier de menukaart van je restaurant</p>
        </div>
        <?php 
       
        // Get all sites in network
        $restaurants = get_sites();
    
        // If there are sites in network
        if ($restaurants) :
           
            foreach( $restaurants as $restaurant ) :

                // If sites theme is 'menukaart' to filter restaurants
                if(get_blog_option($restaurant->blog_id, 'template') == 'menukaart') :
           
                $restaurant_name = get_blog_details($restaurant->blog_id)->blogname;
                $restaurant_link = get_blog_details($restaurant->blog_id)->siteurl;
                ?>
                <div class="bg-light border rounded shadow-sm text-center intro mt-3" style="max-width: 475px;">
                    <div class="p-3">
                        <a style="display-block" href="<?php echo $restaurant_link; ?>" target="_blank">
                            <h4 class="text-left text-decoration-none text-dark"><?php echo $restaurant_name; ?></h4>
                        </a>
                    </div>
                </div>  
                <?php
                endif;
            endforeach;
        endif;
        ?>                 

        <div class="bg-light border rounded shadow-sm text-center intro mt-3" style="max-width: 475px;">
            <div class="p-3">
                    <h4 class="text-left text-decoration-none text-muted">Hier jullie restaurant?</h4>
                <p class="text-left"><a href="<?php home_url(); ?>/contact">Neem contact op</a></p>
            </div>
        </div>

    </div>
</div>

<?php 
get_footer();