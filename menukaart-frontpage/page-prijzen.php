<?php
get_header();
?>

<!-- Header  -->
<div class="features-boxed" style="background-color: rgba(0,0,0,0);">
    <div class="container pb-5">
            <h2 class="text-center">Prijzen</h2>
            <p class="text-center">De eenmalige kosten voor het opmaken van je digitale menukaart bedraagt €299,-.<br>Je krijg van ons direct toegang tot het platform om je digitale menukaart te vullen.</p> 
            <p class="text-center">Tevens ontvang je 50 tafelkaartjes met de QR-code die verwijst naar jouw menukaart!</p>
    </div>
</div>

<!-- Pricing cards -->
<div class="container">
    <div class="card-deck mb-3 text-center mr-5 ml-5">
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Basic</h4>
                <p>Digitale menukaart</p>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">€19,95 <small class="text-muted">/ mnd</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>tot 500 menukaart items</li>
                    <li>tot 50 categoriën</li>
                    <li>eenvoudig aanpasbaar</li>
                    <li>inclusief 50 tafelkaartjes</li>
                    <li>inclusief persoonlijke QR-code</li>
                </ul>
                <a href="<?php home_url(); ?>/contact"><button type="button" class="btn btn-lg btn-block btn-primary">Direct aan de slag!</button></a>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Pro</h4>
                <p>Digitale kaart inclusief bestelmodule</p>
            </div>
        <div class="card-body">
            <h1 class="card-title pricing-card-title">€59,95 <small class="text-muted">/ mnd</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>alle basic mogelijkheden</li>
                    <li>aansturen van Epson printers</li>
                    <li>eenvoudig betalingen ontvangen</li>
                    <li>eenvoudig beheersysteem</li>
                    <li>los in te zetten systeem</li>
                </ul>
                <a href="<?php home_url(); ?>/contact"><button type="button" class="btn btn-lg btn-block btn-primary">Direct aan de slag!</button></a>
            </div>
        </div>
    </div>
</div>

<!-- Features -->
<section class="bg-dark">
    <div class="container p-5 z-depth-1 unique-color-dark">
        <section class="text-center text-light">
            <!-- Section heading -->
            <h2 class="mb-4 pb-2">Een digitale menukaart</h2>
            <p class="lead mx-auto mb-5 text-muted">Een online platform voor het digitaliseren van je menukaart. <br>Je menukaart eenvoudig online zetten en aanpassen.</p>
            <!-- Grid row -->
            <div class="row">
                <!-- Grid column -->
                <div class="col-md-4 mb-4">
                    <i class="fas fa-brain fa-3x purple-text"></i>
                    <h5 class="my-4">Altijd up-to-date</h5>
                    <p class="mb-md-0 mb-5 text-muted">Onze app word continue verbeterd! Er komen steeds meer mogelijkheden beschikbaar. Uiteraard is dit ook op aanvraag!</p>
                </div>
                <div class="col-md-4 mb-4">
                    <i class="fab fa-sass fa-3x purple-text"></i>
                    <h5 class="my-4">Altijd bij de hand</h5>
                    <p class="mb-md-0 mb-5 text-muted">Door het gebruik van QR-codes heb je de menukaart zo bij de hand. Je klanten hoeven hem alleen maar te scannen of simpelweg te navigeren naar de aangegeven URL.</p>
                </div>
                <div class="col-md-4 mb-4">
                    <i class="fas fa-users fa-3x purple-text"></i>
                    <h5 class="my-4">Corona-proof</h5>
                    <p class="mb-0 text-muted">Een fysieke menukaart is niet hygiënisch. Door deze manier van werken beperk je de kans op verspreiding van virussen.</p>
                </div>
            </div>
        </section>
    </div>
</section>

<!-- Extra information -->
<div class="container p-5 z-depth-1 unique-color-dark">
    <section class="text-center">
        <!-- Section heading -->
        <h2 class="mb-4 pb-2">Direct aan de slag?</h2>
        <p class="lead mx-auto mb-5 text-muted">Een online platform voor het digitaliseren van je menukaart. <br>Je menukaart eenvoudig online zetten en aanpassen.</p>
        <!-- Grid row -->
        <div class="row">
            <!-- Grid column -->
            <div class="col-md-4 mb-4">
                <i class="fas fa-brain fa-3x purple-text"></i>
                <h5 class="my-4">Altijd up-to-date</h5>
                <p class="mb-md-0 mb-5 text-muted">Onze app word continue verbeterd! Er komen steeds meer mogelijkheden beschikbaar. Uiteraard is dit ook op aanvraag!</p>
            </div>
            <div class="col-md-4 mb-4">
                <i class="fab fa-sass fa-3x purple-text"></i>
                <h5 class="my-4">Altijd bij de hand</h5>
                <p class="mb-md-0 mb-5 text-muted">Door het gebruik van QR-codes heb je de menukaart zo bij de hand. Je klanten hoeven hem alleen maar te scannen of simpelweg te navigeren naar de aangegeven URL.</p>
            </div>
            <div class="col-md-4 mb-4">
                <i class="fas fa-users fa-3x purple-text"></i>
                <h5 class="my-4">Corona-proof</h5>
                <p class="mb-0 text-muted">Een fysieke menukaart is niet hygiënisch. Door deze manier van werken beperk je de kans op verspreiding van virussen.</p>
            </div>
        </div>
    </section>
</div>

<?php
get_footer();
?>