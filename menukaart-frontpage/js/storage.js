var cart = [];
function addToCart(title, price) {

	var qty = 1;

	// update qty if product is already present
	for (var i in cart) {
		if(cart[i].Product == title){
			cart[i].Qty = qty;
			showCart();
			saveCart();
			return;
		}
	}
	// create JavaScript Object
	var item = { Product: title, Qty: qty, Price: price };
	cart.push(item);
	saveCart();
	showCart();
};

if (localStorage.cart){
	cart = JSON.parse(localStorage.cart);
	showCart();
}

function deleteItem(index){
	cart.splice(index,1); // delete item at index
	showCart();
	saveCart();
}

function saveCart() {
	if ( window.localStorage)
	{
		localStorage.cart = JSON.stringify(cart);
	}
}

function showCart() {
	if (cart.length == 0) {
		jQuery("#cart").css("visibility", "hidden");
		return;
	}

	jQuery("#cart").css("visibility", "visible");
	jQuery("#cartBody").empty();
	for (var i in cart) {
		var item = cart[i];
		var row = "<tr><td>" + item.Product + "</td><td>" + item.Qty + "</td><td>"
						+ item.Qty * item.Price + "</td><td>"
						+ "<button onclick='deleteItem(" + i + ")'>Delete</button></td></tr>";
		jQuery("#cartBody").append(row);
	}
};
