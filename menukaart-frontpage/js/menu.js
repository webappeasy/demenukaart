/* CHOICE */
// Beverage-Menu-Information choice functions.
function filterSelection(className) {
  jQuery('.filter-div').each(function() {
    jQuery(this).removeClass('d-block').addClass('d-none')
  })
  jQuery(className).removeClass('d-none').addClass('d-block')
}

// Add active class to button.
jQuery('.btn').click(function () {
  jQuery('.btn').removeClass('active')
  jQuery(this).addClass('active')
})

/* ALLERGENS */
// Allergen toggle function
function toggleFunction() {
  var checkBox = document.getElementById("formCheck-1");
  var allergen = document.getElementsByClassName("allergen-div");

  console.log(allergen);

  if (jQuery(checkBox).is(':checked')) {
    jQuery(allergen).each(function() {
      jQuery(this).addClass('d-flex').removeClass('d-none')
    })
  } 
  else {
    jQuery(allergen).each(function() {
      jQuery(this).addClass('d-none').removeClass('d-flex')
    })
  }
}