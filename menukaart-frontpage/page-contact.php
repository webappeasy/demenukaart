<?php
get_header();
?>
<!-- Header -->
<div class="header-dark" style="background-image: linear-gradient(0deg, rgba(0,0,0,0.7), rgba(0,0,0,0.5)), url('./wp-content/themes/menukaart-frontpage/assets/img/austin-distel-2vCqH34PqWs-unsplash.jpg');background-repeat: no-repeat, repeat;background-size: cover;background-position: center;">
    <div class="container pt-5">
        <div class="row pt-5">
            <div class="col-md-8 offset-md-2 mt-5 text-light text-center">
            <h2 class="display-4">Contact opnemen</h2>
            <h3 class="h4">We staan je graag te woord.</h3>
            </div>
        </div>
    </div>
</div>
<!-- Contact information -->
<div class="features-boxed bg-dark text-light">
    <div class="container">
        <section>
            <div class="container pt-5 pb-5">
                <div class="row text-center">
                <div class="col-sm-4 mb-3 mb-sm-0">
                    <h3 class="h2">Bezoeken</h3>
                    <span>
                    Roelandsweg 3c
                    <br>4325 CR Renesse
                    </span>
                </div>
                <div class="col-sm-4 mb-3 mb-sm-0">
                    <h3 class="h2">Email</h3>
                    <a href="mailto:mail@demenukaart.app">mail@demenukaart.app</a>
                </div>
                <div class="col-sm-4 mb-3 mb-sm-0">
                    <h3 class="h2">Telefoon</h3>
                    <span>
                    0111 - 407419
                    </span>
                    <div class="text-small text-muted">ma - vr | 08.00 - 17.00</div>
                </div>
                </div>
            </div>
        </section>
        </div>
    </div>
</div>
<!-- Google Maps -->
<div class="container">
    <div class="row justify-content-around">
        <div class="col-md-8 pt-5">
        <div class="shadow" style="width:100%;height:260px;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2471.154784749207!2d3.7704049159777777!3d51.730204202208576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c48d08baa5077d%3A0x5c05e382937b1c10!2sWeb%20%26%20App%20Easy%20B.V.!5e0!3m2!1snl!2snl!4v1588587434439!5m2!1snl!2snl" width="100%" height="100%" frameborder="0px" style="border-radius:5px;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
        </div>
        <div class="col-md-4 col-lg-3 pt-5">
            <h2>Renesse</h2>
            <div class="mb-3">
            <h6 class="mb-1">Postadres</h6>
            <address>
                Roelandsweg 3c
                <br>4335 CR Renesse
            </address>
            </div>
            <div class="mb-3">
            <h6 class="mb-1">Telefoon</h6>
            <span>0111 - 407419</span>
            </div>
            <div class="mb-3">
            <h6 class="mb-1">E-mail</h6>
            <a href="mailto:mail@demenukaart.app">mail@demenukaart.app</a>
            </div>
        </div>
    </div>
</div>
<!-- Tabs differt forms -->
<div class="contact-clean bg-dark">
    <div class="container mx-auto text-light p-5 pb-5 mt-5">
        <h2 class="text-center pb-1">Contactformulier of direct starten?</h2>
        <p class="text-center pb-3">Stel hier gemakkelijk je vragen of vul het aanmeldformulier in.</p>
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link text-light active" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="true">Contactform</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-light" id="pills-signup-tab" data-toggle="pill" href="#pills-signup" role="tab" aria-controls="pills-signup" aria-selected="false">Direct aanmelden</a>
        </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                <?php echo do_shortcode( '[gravityforms id=1 title=false]' ); ?>
            </div>
            <div class="tab-pane fade" id="pills-signup" role="tabpanel" aria-labelledby="pills-signup-tab">
                <?php echo do_shortcode( '[gravityforms id=2 title=false]' ); ?>
            </div>
        </div>
    </div>
</div>
    
<?php
get_footer();
?>