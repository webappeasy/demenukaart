<!-- Footer -->
<?php if ( wp_is_mobile() == false ) { ?>
        <footer class="footer bg-dark mt-auto py-3 fixed-bottom">
        <div class="container text-center">
            <span class="text-light">&copy 2020 demenukaart.app is een product van: <a href="https://www.webandappeasy.com" target="_blank">Web & App Easy B.V.</a></span>
        </div>
        </footer>
    <?php } else { ?>
        <footer class="footer bg-dark mt-auto py-3">
        <div class="container text-center">
            <span class="text-light">&copy 2020 demenukaart.app is een product van: <a href="https://www.webandappeasy.com" target="_blank">Web & App Easy B.V.</a></span>
        </div>
        </footer>
    <?php } ?>
    
</body>
<?php 
wp_footer();
?>