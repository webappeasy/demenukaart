jQuery(document).ready(function($){
    $('[data-toggle="lightbox"]').click(function(event){
        event.preventDefault();
        $(this).ekkoLightbox();
    });
});

// Add Bootstrap classes to cookiebanner button
function myFunction() {
    var element = document.getElementById("accept-cookie");
    element.classList.add("btn btn-success");
  }