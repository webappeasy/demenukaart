<?php
wp_head();
?>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Online menukaart voor ieder restaurant - demenukaart.app</title>
</head>
<body id="homepage" class="page-template-default page" style="background-image: linear-gradient(0deg, rgba(0,0,0,0.7), rgba(0,0,0,0.5)), url('./wp-content/themes/menukaart-frontpage/assets/img/austin-distel-2vCqH34PqWs-unsplash.jpg')">
    <div class="cover-container d-flex w-100 h-10 mx-auto flex-column" style="<?php if ( is_user_logged_in() ){ echo 'height: calc(100vh - 32px);'; } else { echo 'height: 100vh;'; } ?>">
        <header class="mb-auto">
            <nav class="navbar navbar-dark navbar-expand-lg p-3" style="width: 100%;">
                <div class="container"><a class="navbar-brand" href="https://demenukaart.app/home/">demenukaart.app</a><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse" id="navcol-1">
                        <ul class="nav navbar-nav mr-auto">
                            <li class="nav-item" role="presentation"><a class="nav-link active" href="<?php home_url(); ?>/home/">Home</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="<?php home_url(); ?>/mogelijkheden">Mogelijkheden</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="<?php home_url(); ?>/restaurants">Restaurants</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="<?php home_url(); ?>/prijzen">Prijzen</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="<?php home_url(); ?>/contact">Contact</a></li>
                        </ul>
                        <?php 
                        // Change login button to dashboard when user is already lopgged in
                        if ( is_user_logged_in() ) { ?>
                            <a class="btn btn-outline-light ml-3" role="button" href="/login">Dashboard</a>
                        <?php }
                        else { ?>
                            <a class="btn btn-outline-light ml-3" role="button" href="/login">Login</a>
                        <?php }
                        ?>
                    </div>
                </div>
            </nav>
        </header>
        <main class="inner cover text-center" role="main">
            <h1 class="text-light mb-5">demenukaart.app</h1>
            <p class="text-light lead">Hét online platform voor het digitaliseren van je menukaart.</p>
            <p class="text-light lead">Je menukaart eenvoudig online zetten en aanpassen.</p><a class="btn btn-light btn-lg m-3" role="button" href="https://demenukaart.app/mogelijkheden">De mogelijkheden</a><a class="btn btn-lg btn-outline-warning m-3" role="button" href="https://demenukaart.app/demo">Demo starten</a>
        </main>
        <footer class="mastfood mt-auto text-center pb-3">
            <div class="container text-center">
                <span class="text-light">&copy 2020 demenukaart.app is een product van: <a href="https://www.webandappeasy.com" target="_blank">Web & App Easy B.V.</a></span>
            </div>
        </footer>

</body>
<?php
wp_footer();
?>