<?php
wp_head();
// Final test comment
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Online menukaart voor ieder restaurant - demenukaart.app</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
</head>

<body id="single-menucard" style="max-width:768px;" class="mx-auto">
<?php
// Getting the Restaurant information headgroup.
$restaurantIformation = get_field('field_5e9dae05e8ed7');
// Getting the Restaurant NAW group fields
$restaurantNaw = $restaurantIformation['restaurant_naw'];
$address = $restaurantNaw['address'];
$postalCode = $restaurantNaw['postal_code'];
$residence = $restaurantNaw['residence'];
// Getting the rest of the restaurant information vars.
$restaurantName = $restaurantIformation['restaurant_name'];
$restaurantLogo = $restaurantIformation['restaurant_logo'];
$phoneNumber = $restaurantIformation['phone_number'];
$email = $restaurantIformation['e_mail'];
$description = $restaurantIformation['restaurant_description'];
// Euro option
$euro = get_field('field_5eac33e47819c');
$banner = get_field('field_5eb50988064d9');
?>

<!-- Head section -->
<div class="bg-light shadow">
    <div class="text-center" style="background-color:<?php echo $banner ?>">
        <img class="logo-image p-2" src="<?php echo $restaurantLogo?>">
    </div> 
    <?php // Restaurant image slider.
    if(have_rows('restaurant_information')) {
        while(have_rows('restaurant_information')) { the_row();

            $images = get_sub_field('field_5e9db182e798a');

            if( $images ): ?>
                <div id="demenukaart">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner bg-dark"><?php
                            $i = 1;
                            // Image slider.
                            foreach( $images as $image ): 
                                if($i == 1){?>
                                    <div class="carousel-item active">
                                        <img class="d-block w-100 h-100 align-middle" src="<?php echo $image; ?>" style="max-height:300px;" alt="Restaurant Image"/>
                                        <div class="fa fa-plus project-overlay"></div>
                                    </div><?php
                                    $i = 2;
                                }else{?>
                                    <div class="carousel-item">
                                        <img class="d-block w-100 h-100 align-middle" src="<?php echo $image; ?>" style="max-height:300px;" alt="Restaurant Image"/>
                                        <div class="fa fa-plus project-overlay"></div>
                                    </div><?php
                                }
                            endforeach; ?>
                            <div class="carousel-caption d-md-block">
                                <h3><?php echo $restaurantName ?></h3>
                                <p><?php echo $description ?></p>
                            </div>
                        </div><?php // Image slider controlls.
                            if (sizeof($images) > 1) {?>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a><?php
                            }?>
                    </div>
                </div><?php
            endif; 
        }
    } ?>
</div>
<!-- Beverage-Menu Choice -->
<div class="inner cover text-center m-2" id="button-container">
    <div class="btn-group" role="group">
        <button class="btn btn-secondary btn-lg active" onclick="filterSelection('.drinks-filter')"><a class="text-light text-decoration-none" href="#demenukaart">Dranken</a></button>
        <button class="btn btn-secondary btn-lg" onclick="filterSelection('.food-filter')"><a class="text-light text-decoration-none" href="#demenukaart"> Eten</a></button>
        <button class="btn btn-secondary btn-lg" onclick="filterSelection('.information-filter')"><a class="text-light text-decoration-none" href="#demenukaart"> Informatie</a></button>
    </div>
</div>

<!-- BEVERAGE SECTION -->
<div class="filter-div drinks-filter d-block">
    <!-- Product nav section -->
    <ul class="nav navbar-nav navbar-nav-scroll sticky-top" style="height: 54px;color: rgb(52,58,64);font-size: 17px;">
        <li class="nav-item bg-dark"><a class="btn nav-link text-light active" href=#demenukaart> Alles</a></li><?php
        if( have_rows('beverages') ): 
            while( have_rows('beverages') ): the_row();

                $beverageCategory = get_sub_field('field_5ea7d99c0b547');
                $beverageCategorySlug = formatUrl($beverageCategory, $sep='-');?>
                
                <li class="nav-item bg-dark"><a class="btn nav-link text-light" href=#<?php echo $beverageCategorySlug; ?> > <?php echo $beverageCategory; ?></a></li><?php

            endwhile;
        endif;?>
    </ul>
    <?php
    // Getting the Beverage field option vars.
    $beverageOptions = get_field('field_5e9da16dc5fba');
    $beverageImageOption = $beverageOptions['beverage_image_option'];
    $beverageShortDescriptionOption = $beverageOptions['beverage_short_description_option'];
    $beverageLongDescriptionOption = $beverageOptions['beverage_long_description_option'];?>

    <!-- Product section --><?php
    if( have_rows('beverages') ): 
        while( have_rows('beverages') ): the_row();

            $beverageCategory = get_sub_field('field_5ea7d99c0b547');
            $beverageCategorySlug = formatUrl($beverageCategory, $sep='-');?>
            
            <div class="bg-light" ><a id="<?php echo $beverageCategorySlug; ?>" class="btn text-left text-dark font-weight-light mt-2" data-toggle="collapse" aria-expanded="true" aria-controls="collapse-2" href="#collapse-2" role="button" style="width: 100%;color: #ffffff;"><?php echo $beverageCategory; ?></a><?php

                if( have_rows('beverage') ): 
                    while( have_rows('beverage') ): the_row();

                        // Beverage repeater group vars. 
                        $beverageImage = get_sub_field('field_5e9d9caa54776');

                        $beverageTitleShort = get_sub_field('field_5eabdaa935351');
                        $beverageTitle = $beverageTitleShort['beverage_title'];
                        $beverageShort = $beverageTitleShort['beverage_short_description'];
                        $beverageLong = get_sub_field('field_5e9d9cd954779');

                        // Getting the row info from the beverage Price group.
                        if( have_rows('beverage_price_group') ):
                            while( have_rows('beverage_price_group') ): the_row();

                                // Beverage Price group vars. 
                                $beverageMultiplePrices = get_sub_field('field_5e9da05ac799f');
                                $beveragePrice = get_sub_field('field_5e9d9ce35477a');
                                $beverageVolume = get_sub_field('field_5e9d9e4d98940');?>
                                
                                <!-- Begin of the Beverage content -->
                                <div class="bg-white list-group mt-2 mb-2 pr-2 pb-2">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col pt-3 products">
                                                <h4><?php echo $beverageTitle?></h4>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-7"> <!-- Left column -->
                                                <?php
                                                if($beverageShortDescriptionOption){// If field is checked.?>
                                                    <h6><?php echo $beverageShort?></h6><?php
                                                }
                                                if($beverageLongDescriptionOption){// If field is checked.?>
                                                    <?php echo $beverageLong?><?php
                                                } ?>
                                            </div>
                                            <div class="col-5 text-right"> <!-- Right column -->
                                                <div class="pb-2 pr-1 text-right">
                                                    <small style="font-size: 14px;"><?php
                                                        
                                                        if($beverageMultiplePrices){ // If field is checked.

                                                            // Getting the row info from the beverage Multiple Price group.
                                                            if( have_rows('beverage_price_volume') ):
                                                                while( have_rows('beverage_price_volume') ): the_row();

                                                                    // beverage Multiple Price group vars. 
                                                                    $beverageMorePrices = get_sub_field('field_5e9da032c799e');
                                                                    $beverageMoreVolumes = get_sub_field('field_5e9da024c799d');

                                                                    // Output with more Prices and Volumes?>
                                                                    <div>
                                                                        <span class="d-inline"><?php 
                                                                            echo $beverageMoreVolumes?>&nbsp;
                                                                        </span>
                                                                        <span class="d-inline"><?php
                                                                            if($euro){?>
                                                                                €<?php
                                                                            }?>&nbsp;<?php echo $beverageMorePrices?>
                                                                        </span>
                                                                    </div>
                                                                    <?php 
                                                                endwhile;
                                                            endif;
                                                        }
                                                        else { // Output with one Price and Volume
                                                            if($beverageVolume){?>
                                                                <span class="d-inline">
                                                                    <?php echo $beverageVolume?>&nbsp;
                                                                </span><?php
                                                            }?>
                                                                <div class="d-inline"><?php
                                                                    if($euro){?>
                                                                        €<?php
                                                                    }?>&nbsp;<?php echo $beveragePrice?>
                                                                </div>
                                                            <?php
                                                        }?>
                                                    </small>
                                                </div>
                                                <div class="pr-1"><?php
                                                    if($beverageImageOption){// If field is checked.
                                                        if($beverageImage){?>
                                                            <a href="<?php echo $beverageImage?>" data-toggle="lightbox" data-title="<?php echo $beverageTitle?>" data-footer="<?php echo $beverageShort?>"><img class="product-image img-thumbnail" src="<?php echo $beverageImage?>" ></a>
                                                            <?php
                                                        } 
                                                    }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of the Beverage content -->
                                <?php  
                            endwhile;
                        endif;
                    endwhile;
                endif;?>
            </div><?php
        endwhile;
    endif;?>
</div>

<!--  MENU SECTION -->
<div class="filter-div food-filter d-none">
        <!-- Product nav section -->
        <ul class="nav navbar-nav navbar-nav-scroll sticky-top" style="height: 54px;color: rgb(52,58,64);font-size: 17px;">
        <li class="nav-item bg-dark"><a class="btn nav-link text-light active" href=#carouselExampleIndicators> Alles</a></li><?php
        if( have_rows('menus') ): 
            while( have_rows('menus') ): the_row();

                $menuCategory = get_sub_field('field_5ea9243689c27');
                $menuCategorySlug = formatUrl($menuCategory, $sep='-');
                ?>
                
                <li class="nav-item bg-dark"><a class="btn nav-link text-light" href=#<?php echo $menuCategorySlug; ?> > <?php echo $menuCategory; ?></a></li><?php

            endwhile;
        endif;?>
    </ul>
    <!-- Toggle Filter section -->
    <div class="d-flex w-100 justify-content-between mt-2 pl-3 pr-3">
        <p class="font-weight-bold" style="font-size:17px;">Filter</p>
        <div class="custom-control custom-checkbox" style="font-size: 17px;"><input class="custom-control-input" type="checkbox" id="formCheck-1" style="width: 30px; height: 30px;" onclick="toggleFunction()"><label class="custom-control-label" for="formCheck-1">Allergenen&nbsp;</label></div>
    </div>
    <?php
    // Getting the menu field option vars.
    $menuOptions = get_field('field_5ea930e17cb37');
    $menuImageOption = $menuOptions['menu_image_option'];
    $menuShortDescriptionOption = $menuOptions['menu_short_description_option'];
    $menuLongDescriptionOption = $menuOptions['menu_long_description_option'];?>

    <!-- Product section --><?php
    if( have_rows('menus') ): 
        while( have_rows('menus') ): the_row();

            $menuCategory = get_sub_field('field_5ea9243689c27');
            $menuCategorySlug = formatUrl($menuCategory, $sep='-');?>
            
            <div class="bg-light" ><a id="<?php echo $menuCategorySlug; ?>" class="btn text-left text-dark font-weight-light mt-2" data-toggle="collapse" aria-expanded="true" aria-controls="collapse-2" href="#collapse-2" role="button" style="width: 100%;color: #ffffff;"><?php echo $menuCategory; ?></a><?php

                if( have_rows('menu') ): 
                    while( have_rows('menu') ): the_row();

                        $allergens = get_sub_field('field_5ea964977df43');
                        
                        if( have_rows('menu_spec') ): 
                            while( have_rows('menu_spec') ): the_row();

                                // menu repeater group vars. 
                                $menuImage = get_sub_field('field_5ea9256089c29');

                                $menuTitleShort = get_sub_field('field_5eabe062c9f1f');
                                $menuTitle = $menuTitleShort['menu_title'];
                                $menuShort = $menuTitleShort['menu_short_description'];

                                $menuLong = get_sub_field('field_5ea925a1f677c');
                                
                                // Getting the row info from the menu Price group.
                                if( have_rows('menu_price_group') ):
                                    while( have_rows('menu_price_group') ): the_row();

                                        // menu Price group vars. 
                                        $menuMultiplePrices = get_sub_field('field_5ea92645f677e');
                                        $menuPrice = get_sub_field('field_5ea926f1f677f');
                                        $menuVolume = get_sub_field('field_5ea9273bf6780');
                                        ?>
                                        <!-- Begin of the menu content -->
                                        <div class="bg-white list-group mt-2 mb-2 pr-2 pb-2">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col pt-3">
                                                        <h4><?php echo $menuTitle?></h4>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-8"> <!-- Left column -->
                                                        <?php
                                                        if($menuShortDescriptionOption){// If field is checked.?>
                                                        <h6><?php echo $menuShort?></h6><?php
                                                        }
                                                        if($menuLongDescriptionOption){// If field is checked.?>
                                                        <?php echo $menuLong?><?php
                                                        } ?>
                                                    </div>
                                                    <div class="col-4 text-right"> <!-- Right column -->
                                                        <div class="pb-2 pr-1 text-right">
                                                            <small style="font-size: 14px;">
                                                                <?php
                                                                if($menuMultiplePrices){ // If field is checked.

                                                                    // Getting the row info from the menu Multiple Price group.
                                                                    if( have_rows('menu_price_volume') ):
                                                                        while( have_rows('menu_price_volume') ): the_row();

                                                                            // menu Multiple Price group vars. 
                                                                            $menuMorePrices = get_sub_field('field_5ea9279cf6783');
                                                                            $menuMoreVolumes = get_sub_field('field_5ea927acf6784');

                                                                            // Output with more Prices and Volumes?>
                                                                            
                                                                                <span class="d-inline">
                                                                                    <?php echo $menuMoreVolumes?>
                                                                                </span>
                                                                                <span class="d-inline"><?php
                                                                                    if($euro){?>
                                                                                        €<?php
                                                                                    }?><?php echo $menuMorePrices?>
                                                                                </span><br>
                                                                            
                                                                            <?php 
                                                                        endwhile;
                                                                    endif;
                                                                }
                                                                else { // Output with one Price and Volume 
                                                                    if($menuVolume){?>
                                                                        <span class="d-inline">
                                                                            <?php echo $menuVolume?>
                                                                        </span><?php
                                                                    }?>
                                                                        <span class="d-inline"><?php
                                                                            if($euro){?>
                                                                                €<?php
                                                                            }?><?php echo $menuPrice?>
                                                                        </span>
                                                                    <?php
                                                                }?>
                                                            </small>
                                                        </div>
                                                        <div class="pr-1"><?php
                                                            if($menuImageOption){// If field is checked.
                                                                if($menuImage){?>
                                                                    <a href="<?php echo $menuImage?>" data-toggle="lightbox" data-title="<?php echo $menuTitle?>" data-footer="<?php echo $menuShort?>"><img class="product-image img-thumbnail" src="<?php echo $menuImage?>" ></a>
                                                                    <?php
                                                                } 
                                                            }?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="allergen-div flex-wrap w-100 pt-2 pb-1 pl-2 d-none"><?php
                                                    foreach($allergens as $allergen){?>
                                                        <div class="d-flex flex-wrap align-content-between">
                                                            <img style="max-width:50px" class="allergen-image" src="/wp-content/themes/demenukaart/assets/img/<?php echo $allergen?>.png">
                                                        </div><?php
                                                    }
                                                    ?>  
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End of the menu content -->
                                        <?php  
                                    endwhile;
                                endif;
                            endwhile;
                        endif;
                    endwhile;
                endif;?>
            </div><?php
        endwhile;
    endif;?>
</div>

<!--  INFORMATION SECTION -->
<div class="filter-div information-filter d-none">
    <div class="card">
        <div class="card-body mb-5 pb-1"><?php
            if($address){?>
                <p class="d-inline-flex inner"><b>Adres:&nbsp;&nbsp;</b><?php echo $address; ?>&nbsp;<?php echo $postalCode;?>&nbsp;<?php echo $residence ?></p><br><?php
            }?>
                <p class="d-inline-flex inner"><b>Telefoonnummer:&nbsp;&nbsp;</b> <?php echo $phoneNumber ?></p><br>

                <p class="d-inline-flex inner"><b>E-mail:&nbsp;&nbsp;</b><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></p><br>
        </div>  
    </div> 
    <div class="bg-dark text-light container container-fluid fixed-bottom text-center pt-3">
        <p>© 2020 demenukaart.app is een product van&nbsp;&nbsp;<a class="d-inline-flex inner font-weight-light" href="https://www.webandappeasy.com" target="_blank">Web &amp; App Easy</a></p>
    </div>
</div>
</body>
<?php
wp_footer();