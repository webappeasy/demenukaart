<?php 
wp_head();
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Digitale menukaart - demenukaart.app</title>
</head>
<body>
    <div class="d-flex mx-auto flex-column">
        <nav class="navbar navbar-light navbar-expand-lg p-3" style="width: 100%;">
            <div class="container"><a class="navbar-brand" href="<?php home_url(); ?>/home/">demenukaart.app</a><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav mr-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link<?php if (is_page('Home')) echo ' active'; ?>" href="<?php home_url(); ?>/home/">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link<?php if (is_page('Mogelijkheden')) echo ' active'; ?>" href="<?php home_url(); ?>/mogelijkheden">Mogelijkheden</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link<?php if (is_page('Restaurants')) echo ' active'; ?>" href="<?php home_url(); ?>/restaurants">Restaurants</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link<?php if (is_page('Prijzen')) echo ' active'; ?>" href="<?php home_url(); ?>/prijzen">Prijzen</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link<?php if (is_page('Contact')) echo ' active'; ?>" href="<?php home_url(); ?>/contact">Contact</a></li>
                    </ul>
                    <?php 
                    // Change login button to dashboard when user is already lopgged in
                    if ( is_user_logged_in() ) { ?>
                        <a class="btn btn-outline-dark ml-3" role="button" href="/login">Dashboard</a>
                    <?php }
                    else { ?>
                        <a class="btn btn-outline-dark ml-3" role="button" href="/login">Login</a>
                    <?php }
                    ?>
                </div>
            </div>
        </nav> 
    </div>