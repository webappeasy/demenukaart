jQuery(document).ready(function ($) {

    // Horizontal scroll to nav item
    function horizontalScroll() {

        // Animate scroll
        if ($('#menu-items ul .active').length) {
            $('#menu-items ul').stop().animate({
                scrollLeft: $('#menu-items ul').scrollLeft() + $('#menu-items ul .active').offset().left
            }, 750)
        }
    }

    // Set cookie
    function setCookie(cname, cvalue, exdays) {
        var d = new Date()
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000))
        var expires = "expires=" + d.toUTCString()
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=" + window.location.pathname + ";secure"
    }

    // Get cookie
    function getCookie(cname) {
        var name = cname + "="
        var decodedCookie = decodeURIComponent(document.cookie)
        var ca = decodedCookie.split(';')
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i]
            while (c.charAt(0) == ' ') {
                c = c.substring(1)
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length)
            }
        }
        return ""
    }

    // Get current items on order
    function amountItem(amount) {
        if ($('.order').length) {

            // If amount is more then zero
            if (amount) {

                // Set new amount
                $('.order').text(amount)
            } else {

                // Remove badge and order list
                $('.order').remove()
                $('#order-content').html('')
            }
        } else {
            setTimeout(function () {
                $('#order-overview-tab').append(`<span class="badge badge-pill badge-danger position-absolute order animate__animated animate__flip">${amount}</span>`)
            }, 500)
        }
    }

    // Get current order
    function getOrder(items) {
        var subtotal = 0

        if (items.length) {
            // Erase excisting content
            $('#order-content').html('')

            $(items).each(function (index, item) {

                // If item got multiple options
                var option = ''
                if (item.option.unit.length) {
                    option = `<div class="row"><div class="col-12 small text-truncate">${item.option.unit}</div></div>`
                }

                // If item got choices
                var choices = ''
                if (item.choices) {
                    choices = `<div class="row">`
                    $(item.choices).each(function (index, choice) {
                        choices += `<div class="col-12 small text-truncate">${choice.name}</div>`
                    })
                    choices += `</div>`
                }

                // Count item to subtotal
                subtotal = subtotal + item.totalPrice

                // HTML for each item on the order
                var orderHtml = $(`<!-- Order item -->
                    <div class="col-12 mb-3">
                        <div class="row">
                            <div class="col-auto">
                                <div class="row align-items-center">
                                    <div class="col text-right px-2"><a class="amount-minus btn btn-sm text-primary"><i class="fas fa-minus fa-fw"></i></a></div>
                                    <div class="col-auto amount p-0">${item.amount}</div>
                                    <div class="col px-2"><a class="amount-plus btn btn-sm text-primary"><i class="fas fa-plus fa-fw"></i></a></div>
                                </div>
                            </div>
                            <div class="col p-0 text-truncate mt-1">
                                ${item.name}
                                ${option}
                                ${choices}
                            </div>
                            <div class="col-auto mt-1">
                                € ${(item.totalPrice).toFixed(2)}
                            </div>
                        </div>
                    </div>`).appendTo('#order-content').append(orderHtml)

                // On click minus button order
                orderHtml.find('.amount-minus').click(function (btn) {

                    // If amount is not lower then minimal amount
                    if (item.amount > 1) {
                        item.totalPrice = (item.totalPrice / item.amount)
                        item.amount--
                        item.totalPrice = (item.totalPrice * item.amount)
                    } else {
                        items.splice(index, 1)
                    }

                    // Reset total amount badge
                    var totalAmount = parseInt($('.order').text()) - 1
                    amountItem(totalAmount)

                    if (totalAmount) {
                        // Set order cookie
                        setCookie('current-order', JSON.stringify(items), 7/*.125*/)
                    } else {
                        // Delete order cookie
                        setCookie('current-order', '', 0)
                    }

                    // Refresh order
                    getOrder(items)
                })

                // On click plus button order
                orderHtml.find('.amount-plus').click(function (btn) {

                    // If amount is not higher then maximum amount
                    if (item.amount < 99) {
                        item.totalPrice = (item.totalPrice / item.amount)
                        item.amount++
                        item.totalPrice = (item.totalPrice * item.amount)

                        // Reset total amount badge
                        var totalAmount = parseInt($('.order').text()) + 1
                        amountItem(totalAmount)

                        // Set order cookie
                        setCookie('current-order', JSON.stringify(items), 7/*.125*/)

                        // Refresh order
                        getOrder(items)
                    }
                })
            })

            var orderFooter = $(`<!-- Order footer -->
                <div class="col">
                    <div class="row">
                        <div class="col text-center border-top py-3 mx-3"><strong>${menuData.subTotal}: € ${subtotal.toFixed(2)}</strong></div>
                    </div>

                    <div class="row">
                        <div class="col text-center">
                            <button id="request-order" class="btn btn-lg btn-primary">${menuData.orderNow}</button>
                        </div>
                    </div>
                </div>
            `).appendTo('#order-content').append(orderFooter)
        }
    }

    // Show modals after init
    setTimeout(function () {
        $('#alert').modal('show')
        setTimeout(function () {
            $('#alert:not(.registered)').modal('hide')
        }, 3000)
    }, 500)

    // Change table button countdown
    if ($('#change-table').length) {
        var tableChangeText = $('#change-table').text()
        var counter = 10

        function changeTimer() {
            setTimeout(function () {
                $('#change-table').text(tableChangeText + ' (' + counter + ')')
                if (counter != 0) {
                    counter--
                    changeTimer()
                } else {
                    $('#alert').modal('hide')
                }
            }, 1000)
        }
        changeTimer()
    }

    // Show Payment QR code
    $('#request-qr').click(function () {
        $('#payModal').modal('hide')
        $('#paymentQR').modal('show')
    })

    // Fullscreen opening function
    function openFullscreen() {
        var elem = document.documentElement

        if (elem.requestFullscreen) {
            elem.requestFullscreen()
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen()
        } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen()
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen()
        }
    }

    // Fullscreen closing function
    function closeFullscreen() {
        var elem = document.documentElement

        if (document.exitFullscreen) {
            document.exitFullscreen()
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen()
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen()
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen()
        }
    }

    // URL validation function
    function isValidHttpsUrl(string) {
        let url

        try {
            url = new URL(string)
        } catch (_) {
            return false
        }

        return url.protocol === 'https:'
    }

    // Set the variables used by the QR-Scanner
    var video = document.getElementById('scan-screen');
    var canvasElement = document.getElementById('canvas');
    var canvas = canvasElement.getContext('2d');

    // Draw line function if QR-Code is found
    function drawLine(begin, end, color) {
        canvas.beginPath();
        canvas.moveTo(begin.x, begin.y);
        canvas.lineTo(end.x, end.y);
        canvas.lineWidth = 4;
        canvas.strokeStyle = color;
        canvas.stroke();
    }

    // QR Scan handler
    var animation
    var cam
    function tick() {

        // If video is ready
        if (video.readyState === video.HAVE_ENOUGH_DATA) {

            // Mirror video on canvas
            $('.scan-container').height(window.innerHeight)
            $('#canvas').attr('width', window.innerWidth).attr('height', window.innerHeight)
            canvas.drawImage(video, (0 - (($('#scan-screen').width() - window.innerWidth) / 2)), 0, $('#scan-screen').width(), window.innerHeight)
            var imageData = canvas.getImageData(0, 0, $('#canvas').width(), $('#canvas').height())

            // Init the QR-Scanner
            var code = jsQR(imageData.data, imageData.width, imageData.height, {
                inversionAttempts: 'dontInvert',
            })

            // If QR-Code is found
            if (code) {

                // Draw line around QR-Code
                drawLine(code.location.topLeftCorner, code.location.topRightCorner, '#28a745');
                drawLine(code.location.topRightCorner, code.location.bottomRightCorner, '#28a745');
                drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, '#28a745');
                drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, '#28a745');

                // If QR-Code contains a valid HTTPS URL
                if (isValidHttpsUrl(code.data)) {

                    // Pause the video and redirect
                    video.pause()
                    window.location.href = code.data
                    return
                }
            }
        }
        animation = requestAnimationFrame(tick)
    }

    // Detect function for device cameras
    function detectCamera(callback) {
        let md = navigator.mediaDevices;
        if (!md || !md.enumerateDevices) return callback(false);
        md.enumerateDevices().then(devices => {
            callback(devices.some(device => 'videoinput' === device.kind));
        })
    }

    // Search for any camera on device
    detectCamera(function (hasWebcam) {

        // If camera is found
        if (hasWebcam) {

            // Make QR-Scanner available in menu
            $('#qrcode').removeClass('d-none')
        }
    })

    // On click QR Scanner item in menu
    $('#qrcode').click(function () {

        // Set the camera options, attempt 'environment' to use back camera
        navigator.mediaDevices.getUserMedia({
            video: {
                facingMode: 'environment'
            }
        }).then(function (stream) {
            cam = stream
            video.srcObject = stream;
            video.play();
            animation = requestAnimationFrame(tick)

            $('header, main, footer').addClass('d-none')
            $('.scan-container').removeClass('d-none')
            openFullscreen()
        })
    })

    // On click close QR-Scanner
    $('#close-scanner').click(function () {

        // Stop the camera
        cancelAnimationFrame(animation)
        cam.getTracks().forEach(track => track.stop())
        $('header, main, footer').removeClass('d-none')
        $('.scan-container').addClass('d-none')
        closeFullscreen()
    })

    // Order modal amounts
    var menuResults
    var offsetTop
    var currentOrder = []

    // If orders are set in cookie
    if (getCookie('current-order').length) {
        currentOrder = JSON.parse(getCookie('current-order'))
        getOrder(currentOrder)
    }

    // Print Menu items + Menu overview
    function printMenuItems(menu) {

        // Reset elements html
        $('#menu-items ul, #menu-content').empty()

        // Add html
        $(menu).each(function (index, cat) {
            if (cat.items.length) {
                // Nav items
                $('#menu-items ul').append(`
                    <li class="nav-item">
                        <a class="nav-link p-3 text-nowrap" href="#${cat.slug}">${cat.name}</a>
                    </li>`)

                // Menu category titles html
                var categoryInfo = ''
                if (cat.info.length) {
                    categoryInfo = `<p class="col-12 mb-3"><em>${cat.info}</em></p>`
                }

                var title = $(`<!-- Category title -->
                    <h5 id="${cat.slug}" class="col-12 pt-3 mb-3">${cat.name}</h5>${categoryInfo}`).appendTo('#menu-content').append(title)

                // Menu items html
                $(cat.items).each(function (index, item) {
                    var itemDescription = ''
                    if (item.description) itemDescription = `<p class="card-text text-muted small mt-2">${item.description}</p>`

                    var thumbnail = ''
                    if (item.image) thumbnail = `<div class="col-auto p-0 mr-2 thumb bg-light border rounded-circle" style="background-image: url('${item.image}')"></div>`

                    // Output the html
                    var html = $(`<!-- Menu item -->
                        <div class="col-sm-6 mb-3">
                            <div class="card" data-toggle="modal" data-target="#menuItemModal">
                                <div class="card-body p-2">
                                    <div class="container d-flex p-0">
                                        <div class="row w-100 m-0 align-items-center">
                                            ${thumbnail}
                                            <div class="col p-0">
                                                <div class="row">
                                                    <div class="col"><h6 class="card-title text-break mb-0">${item.name}</h6></div>
                                                    <div class="col-auto small">${item.min_price_text} ${item.min_price.toFixed(2)}</div>
                                                </div>
                                                ${itemDescription}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`).appendTo('#menu-content').append(html)

                    // On click fill modal with correct data
                    html.click(function () {

                        // On modal show
                        $('#menuItemModal').on('show.bs.modal', function (e) {

                            // Reset amount
                            var amount = 1
                            $(e.target).find('.amount').text(amount)

                            // Get allergens
                            var allergens = ''
                            if (item.allergens_content.length) {
                                allergens += `
                                    <h6>${item.allergens_title}</h6>
                                    <div class="row px-3 mb-2 text-center">`

                                $(item.allergens_content).each(function (index, item) {
                                    allergens += `
                                        <div class="col-auto p-0">
                                            <img class="allergen" src="${item.icon}" alt="${item.text}">
                                            <small class="d-block allergen-text">${item.text}</small>
                                        </div>`
                                })

                                allergens += `</div>`
                            }

                            // Get choices
                            var choices = ''
                            if (item.choices_content.length) {
                                choices += `<h6 class="mt-2">${item.choices_title}</h6>`

                                $(item.choices_content).each(function (index, item) {
                                    if ($('#menuItemModal .modal-footer').length) {
                                        choices += `
                                            <div class="btn btn-light p-3 mb-2 w-100">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="${item.slug}">
                                                    <label class="custom-control-label w-100" for="${item.slug}">
                                                        <div class="row align-items-center">
                                                            <div class="col text-left">${item.name}</div>`
                                        if (item.price) {
                                            choices += `<div class="col-auto"><small>+ ${item.price.toFixed(2)}</small></div>`
                                        }
                                        choices += `</div>
                                                    </label>
                                                </div>
                                            </div>
                                        `
                                    } else {
                                        choices += `
                                            <div class="btn btn-light p-3 mb-2 w-100">
                                                <div class="row align-items-center">
                                                    <div class="col text-left">${item.name}</div>`
                                        if (item.price) {
                                            choices += `<div class="col-auto"><small>+ ${item.price.toFixed(2)}</small></div>`
                                        }
                                        choices += `</div>
                                                </div>
                                            `
                                    }
                                })

                                choices += `</div>`
                            }

                            // Get options
                            var options = ''
                            if (item.options.length > 1) {
                                options += `<h6 class="mt-2">${item.options_title}</h6>`

                                $(item.options).each(function (index, item) {
                                    if ($('#menuItemModal .modal-footer').length) {
                                        var checked = (index == 0) ? 'checked' : ''
                                        options += `
                                            <div class="btn btn-light p-3 mb-2 w-100">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="option" value="${item.slug}" class="custom-control-input" id="${item.slug}" ${checked}>
                                                    <label class="custom-control-label w-100" for="${item.slug}">
                                                        <div class="row align-items-center">
                                                            <div class="col text-left">${item.unit}</div>`
                                        if (item.price) {
                                            options += `<div class="col-auto"><small>${item.price.toFixed(2)}</small></div>`
                                        }
                                        options += `</div>
                                                    </label>
                                                </div>
                                            </div>
                                        `
                                    } else {
                                        options += `
                                            <div class="btn btn-light p-3 mb-2 w-100">
                                                <div class="row align-items-center">
                                                    <div class="col text-left">${item.unit}</div>`
                                        if (item.price) {
                                            options += `<div class="col-auto"><small>${item.price.toFixed(2)}</small></div>`
                                        }
                                        options += `</div>
                                                </div>
                                            `
                                    }
                                })

                                options += `</div>`
                            }

                            // Print Modal for item
                            $(e.target).find('.modal-body').html(`
                                <h5 class="modal-title">${item.name}</h5>
                                <p class="text-muted small">${item.description}</p>
                                ${allergens}
                                ${options}
                                ${choices}
                            `)

                            if ($(e.target).find('.modal-footer').length) {
                                $(e.target).find('.modal-footer').html(`
                                    <div class="w-100">
                                        <div class="row align-items-center mb-3">
                                            <div class="col text-right"><a class="amount-minus btn text-primary"><i class="fas fa-minus fa-fw"></i></a></div>
                                            <div class="col-auto amount">1</div>
                                            <div class="col"><a class="amount-plus btn text-primary"><i class="fas fa-plus fa-fw"></i></a></div>
                                        </div>
                                    </div>
                                    <button id="orderBtn" type="button" class="btn btn-lg btn-primary" data-dismiss="modal">${menuData.addItem}</button>
                                `)
                            }

                            // On click minus button
                            $(e.target).find('.amount-minus').click(function (btn) {

                                // If amount is not lower then minimal amount
                                if (amount > 1) {
                                    amount--
                                    $(btn.target).closest('.row').find('.amount').text(amount)
                                }
                            })

                            // On click plus button
                            $(e.target).find('.amount-plus').click(function (btn) {

                                // If amount is not higher then maximum amount
                                if (amount < 99) {
                                    amount++
                                    $(btn.target).closest('.row').find('.amount').text(amount)
                                }
                            })

                            // On click order button
                            $('#orderBtn').click(function () {
                                var addedItem = {}
                                var totalPrice = 0

                                // Get the required choosen option
                                if (item.options.length) {
                                    $(item.options).each(function (index, option) {
                                        if (option.slug && $(e.target).find(`#${option.slug}`).length && $(e.target).find(`#${option.slug}`)[0].checked) {
                                            addedItem['option'] = option
                                            totalPrice = option.price
                                        } else {
                                            addedItem['option'] = item.options[0]
                                            totalPrice = item.options[0].price
                                        }
                                    })
                                }

                                // Get the optional choosen choices
                                if (item.choices_content.length) {
                                    addedItem['choices'] = []
                                    $(item.choices_content).each(function (index, choice) {
                                        if ($(e.target).find(`#${choice.slug}`).length && $(e.target).find(`#${choice.slug}`)[0].checked) {
                                            addedItem['choices'].push(choice)
                                            totalPrice = totalPrice + choice.price
                                        }
                                    })
                                }

                                addedItem['amount'] = amount
                                addedItem['name'] = item.name
                                addedItem['totalPrice'] = amount * totalPrice

                                var totalAmount = ($('.order').text()) ? parseInt($('.order').text()) + amount : amount

                                // Push added item to current order
                                currentOrder.push(addedItem)

                                // Add to order and change total amount
                                getOrder(currentOrder)
                                amountItem(totalAmount)

                                // Set order cookie
                                setCookie('current-order', JSON.stringify(currentOrder), 7/*.125*/)
                            })
                        })
                    })
                })
            }

        }).promise().done(function () {

            // Get offset needed for top
            offsetTop = $('header').height() + $('#menu-items').height() + $('.search-bar').outerHeight()

            // Dispose excisting scrollspy first
            $('body').scrollspy('dispose')

            // Init new scrollspy
            $('body').scrollspy({
                target: '#menu-items',
                offset: offsetTop
            })

            // On Scrollspy change, scroll menu items
            $(window).on('activate.bs.scrollspy', function () {
                horizontalScroll()
            })

            // On menu items click
            $('#menu-items ul a').click(function (e) {

                // Prevent html scroll to ID
                e.preventDefault()
                var body = $('html, body')

                // Animate scroll to the desired ID
                body.stop().animate({
                    scrollTop: ($($(e.target).attr('href')).position().top - offsetTop) + 2
                }, 750)
            })
        })
    }

    // Get the menu overview
    $.ajax({
        beforeSend: xhr => {
            xhr.setRequestHeader('X-WP-Nonce', menuData.nonce)
        },
        url: menuData.root + 'menu/v1/overview',
        type: 'GET',
        data: {
            lang: menuData.lang
        },
        success: response => {
            menuResults = response

            // On success load menu overview
            printMenuItems(menuResults)
        },
        error: response => {
            $('#menu-overview').html(response.responseText)
            console.log(response.responseText)
        }
    })

    // On change Searchbar
    var searchTimeout
    $('#search').keyup(function () {
        // Clear timeout
        clearTimeout(searchTimeout)

        // Trim the search value whitspaces
        var searchValue = $.trim($('#search').val().toLowerCase())

        // Activate filter after half a second
        searchTimeout = setTimeout(function () {
            var filteredMenu = []

            // If search value has length after trim
            if (searchValue.length) {

                // Filter through items
                $(menuResults).each(function (index, category) {
                    const filteredItems = category.items.filter(function (item, i) {
                        if (item.name.toLowerCase().includes(searchValue) || item.description.toLowerCase().includes(searchValue)) {
                            return true
                        } else {
                            return false
                        }
                    })

                    // Make new filtered array
                    var filteredCategory =
                    {
                        'id': category.id,
                        'slug': category.slug,
                        'info': category.info,
                        'name': category.name,
                        'items': category.items
                    }

                    filteredCategory.items = filteredItems
                    filteredMenu[index] = filteredCategory
                })

                // Print filtered menu
                printMenuItems(filteredMenu)
            } else {

                // Print original menu
                printMenuItems(menuResults)
            }
        }, 500)
    })

    // On click reset button for search input
    $('#reset-search').click(function () {
        $('#search').val('').trigger('keyup')
    })

    // On click order now button
    $('#request-order').click(function () {
        alert('joe')
    })
})