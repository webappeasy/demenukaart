jQuery(document).ready(function ($) {
    if ($('#example-table').length) {
        function getSum(total, num) {
            var t = parseFloat(total);
            var n = parseFloat(num);
            var f = t + n;
            return f.toFixed(2);
        }

        // Calculate total exclude function
        var exclCalc = function (values, data, calcParams) {
            return data.map(function (d) {

                var c = parseFloat(d.sell).toFixed(2);
                var k = parseInt(d.status);
                var e = parseInt(d.qty);
                var g = parseInt("100");
                var j = parseFloat(k + g);
                var f = c / j * g * e;
                return f;
            })
                .reduce(getSum, 0);
        }
        // Calculate total btw9 function
        var btwCalc = function (values, data, calcParams) {
            return data.map(function (d) {
                var c = parseFloat(d.sell).toFixed(2);
                var k = parseInt(d.status);
                var e = parseInt(d.qty);
                var g = parseInt("100");
                var j = k + g;
                var f = c / j * g * e;
                var h = f / g * j - f;
                return h;
            })
                .reduce(getSum, 0);
        }
        // Calculate total include function
        var inclCalc = function (values, data, calcParams) {
            return data.map(function (d) {
                return d.sell * d.qty;
            })
                .reduce(getSum, 0);
        }

        // Static data for products
        var tableData = [
            { id: 1, cat: "Koude dranken", name: "Coca Cola klein", buy: "0.56", sell: "2.80", status: "9", qty: "10", btw: "2.31" },
            { id: 2, cat: "Koude dranken", name: "Coca Cola normaal", buy: "0.72", sell: "3.60", status: "9", qty: "23", btw: "6.84" },
            { id: 3, cat: "Koude dranken", name: "Coca Cola groot", buy: "1.10", sell: "5.50", status: "9", qty: "4", btw: "1.82" },
            { id: 4, cat: "Warme dranken", name: "Koffie", buy: "0.53", sell: "2.80", status: "9", qty: "22", btw: "5.09" },
            { id: 5, cat: "Wijnen", name: "Huiswijn wit glas", buy: "0.60", sell: "4.50", status: "21", qty: "8", btw: "6.25" },
            { id: 6, cat: "Wijnen", name: "Huiswijn wit fles", buy: "3.00", sell: "22.50", status: "21", qty: "1", btw: "3.90" },
        ]

        //Define variables for input elements
        var fieldEl = document.getElementById("filter-field");
        var typeEl = document.getElementById("filter-type");
        var valueEl = document.getElementById("filter-value");

        // Tabulator overview table code.
        var table = new Tabulator("#example-table", {
            height: "311px",
            layout: "fitColumns",
            movableRows: true,
            columns: [
                { title: "Naam", field: "name", width: 200 },
                { title: "inkoop", field: "buy", hozAlign: "center", width: 100 },
                { title: "verkoop", field: "sell", hozAlign: "center", width: 100 },
                { title: "BTW-Status", field: "status", width: 200 },
                { title: "Hoeveelheid", field: "qty", width: 200, bottomCalc: "sum" },
                {
                    title: "Totaal excl.", field: "excl", width: 200, bottomCalc: exclCalc, formatter: function (cell) {
                        var data = cell.getData();
                        var c = parseFloat(data.sell).toFixed(2);
                        var d = parseFloat(data.status);
                        var e = parseFloat(data.qty).toFixed(2);
                        var g = parseInt("100");
                        var j = d + g;
                        var f = c / j * g * e;
                        return f.toFixed(2);
                    }
                },
                {
                    title: "BTW", field: "btw", width: 200, bottomCalc: "sum" /*, formatter:function(cell)
                    {
                        var data = cell.getData();
                        var c = parseFloat(data.sell).toFixed(2);
                        var d = parseFloat(data.status);
                        var e = parseFloat(data.qty).toFixed(2);
                        var g = parseInt("100");
                        var j = d + g;
                        var f = c / j * g * e;
                        var h = f / g * j - f;
                        return h.toFixed(2);
                        
                    }*/
                },
                {
                    title: "Totaal incl.", field: "incl", hozAlign: "center", bottomCalc: inclCalc, formatter: function (cell) {
                        var data = cell.getData();
                        var a = parseFloat(data.sell).toFixed(2);
                        var b = parseFloat(data.qty).toFixed(2);
                        c = (a * b).toFixed(2);
                        return c;
                    }
                }
            ],
        });
        table.setData(tableData);
        //trigger download of data.xlsx file
        document.getElementById("download-xlsx").addEventListener("click", function () {
            table.download("xlsx", "data.xlsx", { sheetName: "Mijn overzicht" });
        });
        //trigger download of data.pdf file
        document.getElementById("download-pdf").addEventListener("click", function () {
            table.download("pdf", "data.pdf", {
                orientation: "portrait", //set page orientation to portrait
                title: "Report", //add title to report
            });
        });
        //Trigger setFilter function with correct parameters
        function updateFilter() {
            var filterVal = fieldEl.options[fieldEl.selectedIndex].value;
            var typeVal = typeEl.options[typeEl.selectedIndex].value;

            var filter = filterVal == "function" ? customFilter : filterVal;

            if (filterVal == "function") {
                typeEl.disabled = true;
                valueEl.disabled = true;
            } else {
                typeEl.disabled = false;
                valueEl.disabled = false;
            }

            if (filterVal) {
                table.setFilter(filter, typeVal, valueEl.value);
            }
        }

        //Update filters on value change
        document.getElementById("filter-field").addEventListener("change", updateFilter);
        document.getElementById("filter-type").addEventListener("change", updateFilter);
        document.getElementById("filter-value").addEventListener("keyup", updateFilter);

        //Clear filters on "Clear Filters" button click
        document.getElementById("filter-clear").addEventListener("click", function () {
            fieldEl.value = "";
            typeEl.value = "=";
            valueEl.value = "";

            table.clearFilter();
        });
    }

});