��    e      D  �   l      �  (   �     �     �     �     �  
   �     �     	     	     	     -	     <	     I	  	   Y	     c	      t	  :   �	     �	     �	     �	     �	     �	     
     #
     5
     A
     O
     a
     y
     �
  +   �
     �
  
   �
     �
  	   �
     �
     �
               .     B  ;   J     �     �     �     �     �     �     �     �            )   #      M     n          �     �     �     �     �     �     �  	   �     �       
        *     ;     H  	   Q     [     i     n  	   �     �     �     �     �     �     �     �     �       
                  .     =     J     [     g     x     �     �     �     �     �     �     �    �  '        .  	   :     D  
   Q  	   \     f     o     w  	   �     �  
   �     �  	   �     �     �  /   �           $     1     ?  -   F     t     �     �     �     �     �     �     �  '        +  
   2     =     B     G     S     Y     b     q     �  7   �     �     �  
   �     �     �       	              1  	   6  $   @     e     �     �     �     �     �     �     �     �     �     �            
        )  	   6     @     G     N     \     a     u     ~     �     �     �     �     �     �     �     �     �     �  	   �       
             .     =     J     Z     i     w     ~     �     �     �     @   :                      0   T   =      D   W             Y   ;   J   a   +   E      9   U   (                         "   )   #              8   B                     C   1   -       ,      H       e                 7   	           ^   Q       4      A           G       K   N   L   $   &                 X   ?   Z         _               ]   P      *       F   2       [   \   '   .   R           >   b      O   S   <   c      `   !   I   M   3   %             V   d      /   5          
       6                 <strong>FOUT</strong>: Ongeldige Captcha Alle keuzes Alle menukaarten Alle producten Alle tafels Allergenen BTW hoog BTW laag Bekijk keuze Bekijk menukaart Bekijk product Bekijk tafel Contact support Dashboard Download QR-Code Download QR-Code voor deze tafel Een nieuwe manier om geld te verdienen via uw siteverkeer. Ei Extra keuze Extra keuzes Gluten Helaas, deze tafel is al bezet Hogere resolutie Huidige menukaart Inkoopprijs Inkoopprijzen Internetbankieren Je zit al aan een tafel Keuze aanmaken Kies een betaalmethode Laat de QR-code door een medewerker scannen Lupine Management Melk Menukaart Menukaart aanmaken Menukaarten Mijn bestelling Mollie API-sleutel Mollie instellingen Mosterd Na het publiceren van de tafel verschijnt hier een QR-Code. Nieuw product Nieuw product maken Nieuwe keuze Nieuwe keuze maken Nieuwe menukaart Nieuwe menukaart maken Nieuwe tafel Nieuwe tafel maken Noten Nu bestellen Om internetbankieren via %s te activeren. Om printers via %s te activeren. Online menukaart Opties Pin of Contant Pinda's Printnode API-sleutel Printnode instellingen Privacyverklaring Product Product aanmaken Producten QR-Code menukaart QR-Code tafel QR-Scanner Rekening betalen Schaaldieren Selderij Sesamzaad Sluit scanner Soja Stel in als huidig menu Subtotaal Succesvol geregistreerd! Tafel Tafel aanmaken Tafels Toevoegen aan bestelling Totaal Verkoopprijs Verkoopprijzen Vis Weekdieren Wijzig keuze Wijzig menukaart Wijzig product Wijzig tafel Wissel van tafel Zoek keuzes Zoek menukaarten Zoek op de kaart Zoek producten Zoek tafels Zwavel hCaptcha geheime sleutel hCaptcha netwerk opties hCaptcha site sleutel v.a. Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Project-Id-Version: WPML_EXPORT_menukaart
PO-Revision-Date: 
Language-Team: 
MIME-Version: 1.0
X-Generator: Poedit 2.3.1
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: en_US
 <strong>ERROR</strong>: Invalid Captcha All choices All menus All products All tables Allergens VAT high VAT low View choice View menu View product View table Contact support Dashboard Download QR Code Download QR Code for this table A new way to make money from your site traffic. Egg Extra choice Extra choices Gluten Unfortunately, this table is already occupied Higher resolution Current menu Purchase price Purchase prices Internet banking You're already at a table Create choice Choose a payment method Have the QR code scanned by an employee Lupine Management Milk Menu Create menu Menus My order Mollie API Key Mollie settings Mustard After publishing the table, a QR Code will appear here. New product Create new product New choice Create new choice New menu Create new menu New table Create new table Nuts Order now To activate internet banking via %s. To activate printers via %s. Online menu Options Pin or Cash Peanuts Printnode API Key Printnode settings Privacy declaration Product Create product Products QR-Code menu QR Code table QR Scanner Pay the bill Shellfish Celery Sesame Close scanner Soya Set as current menu Subtotal Registered successfully! Table Create table Tables Add to order Total Selling price Selling prices Fish Molluscs Edit choice Edit menu Edit product Edit table Switch the table Search choices Search menus Search the menu Search product Search tables Sulfur hCaptcha API Key hCaptcha network options hCaptcha Site Key from 