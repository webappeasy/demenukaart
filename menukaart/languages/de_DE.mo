��    e      D  �   l      �  (   �     �     �     �     �  
   �     �     	     	     	     -	     <	     I	  	   Y	     c	      t	  :   �	     �	     �	     �	     �	     �	     
     #
     5
     A
     O
     a
     y
     �
  +   �
     �
  
   �
     �
  	   �
     �
     �
               .     B  ;   J     �     �     �     �     �     �     �     �            )   #      M     n          �     �     �     �     �     �     �  	   �     �       
        *     ;     H  	   Q     [     i     n  	   �     �     �     �     �     �     �     �     �       
                  .     =     J     [     g     x     �     �     �     �     �     �     �    �  ,        0     D     V     d  	   p     z     �     �     �     �     �     �  	   	       *   )  A   T     �     �     �     �  &   �     �       	     
   "     -     ;     X     o  4   �     �  
   �     �     �     �     �     
          1     F  D   K     �     �     �     �     �     �  
             3     :  0   J  #   {     �     �     �  	   �     �     �               %     7     @     T  
   b     m  
   �     �     �     �     �  $   �     �     �                         9     @     N     ]  
   c     n     �     �     �     �     �     �               3     I     R     j     �     �     @   :                      0   T   =      D   W             Y   ;   J   a   +   E      9   U   (                         "   )   #              8   B                     C   1   -       ,      H       e                 7   	           ^   Q       4      A           G       K   N   L   $   &                 X   ?   Z         _               ]   P      *       F   2       [   \   '   .   R           >   b      O   S   <   c      `   !   I   M   3   %             V   d      /   5          
       6                 <strong>FOUT</strong>: Ongeldige Captcha Alle keuzes Alle menukaarten Alle producten Alle tafels Allergenen BTW hoog BTW laag Bekijk keuze Bekijk menukaart Bekijk product Bekijk tafel Contact support Dashboard Download QR-Code Download QR-Code voor deze tafel Een nieuwe manier om geld te verdienen via uw siteverkeer. Ei Extra keuze Extra keuzes Gluten Helaas, deze tafel is al bezet Hogere resolutie Huidige menukaart Inkoopprijs Inkoopprijzen Internetbankieren Je zit al aan een tafel Keuze aanmaken Kies een betaalmethode Laat de QR-code door een medewerker scannen Lupine Management Melk Menukaart Menukaart aanmaken Menukaarten Mijn bestelling Mollie API-sleutel Mollie instellingen Mosterd Na het publiceren van de tafel verschijnt hier een QR-Code. Nieuw product Nieuw product maken Nieuwe keuze Nieuwe keuze maken Nieuwe menukaart Nieuwe menukaart maken Nieuwe tafel Nieuwe tafel maken Noten Nu bestellen Om internetbankieren via %s te activeren. Om printers via %s te activeren. Online menukaart Opties Pin of Contant Pinda's Printnode API-sleutel Printnode instellingen Privacyverklaring Product Product aanmaken Producten QR-Code menukaart QR-Code tafel QR-Scanner Rekening betalen Schaaldieren Selderij Sesamzaad Sluit scanner Soja Stel in als huidig menu Subtotaal Succesvol geregistreerd! Tafel Tafel aanmaken Tafels Toevoegen aan bestelling Totaal Verkoopprijs Verkoopprijzen Vis Weekdieren Wijzig keuze Wijzig menukaart Wijzig product Wijzig tafel Wissel van tafel Zoek keuzes Zoek menukaarten Zoek op de kaart Zoek producten Zoek tafels Zwavel hCaptcha geheime sleutel hCaptcha netwerk opties hCaptcha site sleutel v.a. Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Project-Id-Version: WPML_EXPORT_menukaart
PO-Revision-Date: 
Language-Team: 
MIME-Version: 1.0
X-Generator: Poedit 2.3.1
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de
 <strong>FALSCH</strong>: Ungültiges Captcha Alle Möglichkeiten Alle Speisekarten Alle Produkte Alle Tische Allergene Mehrwertsteuer hoch Mehrwertsteuer niedrig Möglichkeit anzeigen Speisekarte anzeigen Produkt anzeigen Tisch anzeigen Kontaktieren Sie Support Dashboard Herunterladen QR-Code Herunterladen den QR-Code für diese Tisch Eine neue Möglichkeit, mit Ihrem Site-Traffic Geld zu verdienen. Ei Extra Möglichkeit Extra Möglichkeiten Gluten Leider ist dieser Tisch bereits belegt Höhere Auflösung Aktuelles Speisekarte Kaufpreis Kaufpreise Onlinebanking Du bist schon an einem Tisch Möglichkeit erstellen Wählen Sie eine Bezahlungsart Lassen Sie den QR-Code von einem Mitarbeiter scannen Lupine Verwaltung Milch Speisekarte Speisekarte erstellen Speisekarten Meine Bestellung Mollie API-Schlüssel Mollie Einstellungen Senf Nach dem Veröffentlichen der Tisch wird hier ein QR-Code angezeigt. Neue Produkt Neuen Produkte erstellen Neue Möglichkeit Neuen Möglichkeit erstellen Neue Speisekarte Neuen Speisekarte erstellen Neue Tisch Neuen Tisch erstellen Nüsse Jetzt bestellen So aktivieren Sie das Internet-Banking über %s. So aktivieren Sie Drucker über %s. Online Speisekarte Optionen PIN oder Bargeld Erdnüsse Printnode API-Schlüssel Printnode Einstellungen Datenschutzerklärung Produkt Produkt erstellen Produkte QR-Code Speisekarte QR-Code Tisch QR-Scanner Die Rechnung bezahlen Krebstiere Sellerie Sesam Scanner schließen Soja Als aktuelles Speisekarte einstellen Zwischensumme Erfolgreich registriert! Tisch Tisch erstellen Tische Zur Bestellung hinzufügen Gesamt Verkaufspreis Verkaufspreise Fisch Weichtiere Möglichkeit ändern Speisekarte ändern Produkt ändern Tisch ändern Schalten Sie den Tisch Suchen Sie die Möglichkeiten Suchen Sie die Speisekarten Suchen Sie die Karte Suchen Sie die Produkte Suchen Sie die Tische Schwefel hCaptcha API-Schlüssel hCaptcha-Netzwerkoptionen hCaptcha API-Schlüssel von 