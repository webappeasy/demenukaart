<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Load Header
require get_stylesheet_directory() . '/assets/header.php';
?>

<!-- Index page items -->
<div class="tab-content">
    <div class="tab-pane fade show active" id="menu-overview" role="tabpanel" aria-labelledby="menu-overview-tab">

        <!-- Search form -->
        <div class="input-group px-2 py-2 border-bottom bg-white fixed-top search-bar">
            <div class="input-group-prepend">
                <label for="search" class="input-group-text rounded-0 border-0 bg-white"><i class="fas fa-search"></i></label>
            </div>
            <input type="text" id="search" class="form-control rounded-0 border-0 pl-0" placeholder="<?php _e('Zoek op de kaart', 'menukaart');?>..." autocomplete="off">
            <div class="input-group-append">
                <span id="reset-search" class="input-group-text rounded-0 border-0 bg-white btn"><i class="fas fa-times-circle"></i></span>
            </div>
        </div>
        
        <!-- Menu overview -->
        <nav id="menu-items" class="nav bg-light border-bottom bg-light fixed-top menu-scroller">
            <ul class="nav"></ul>
        </nav>

        <!-- Menu content -->
        <div id="menu-content" class="row"></div>

    </div>

    <?php if(get_option('printnode_api') && is_active_customer()) :?>
        <!-- Order overview -->
        <div class="tab-pane fade" id="order-overview" role="tabpanel" aria-labelledby="order-overview-tab">
            <h5 class="mb-3"><?php _e('Mijn bestelling', 'menukaart');?></h5>
            <div id="order-content" class="row"></div>
        </div>

        <!-- Receipt overview -->
        <div class="tab-pane fade" id="receipt-overview" role="tabpanel" aria-labelledby="receipt-overview-tab">
            <div id="receipt-container" class="bg-light container py-4 my-2 shadow">

                <!-- Receipt head -->
                <div class="col px-0 mb-2">
                        <div class="row">
                            <div class="col text-center mb-2">
                                <?php if(has_custom_logo()) :
                                    $custom_logo_id = get_theme_mod('custom_logo');
                                    $image = wp_get_attachment_url($custom_logo_id , 'full');?>
                                    <img class="receipt-logo" src="<?php echo $image;?>" alt="<?php echo get_bloginfo('name');?>">
                                <?php else :?>
                                    <h3><?php echo get_bloginfo('name');?></h3>
                                <?php endif;?>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col"><strong><?php echo get_the_title(json_decode(stripslashes($_COOKIE['qr-scan']))->table);?></strong></div>
                    </div>
                </div>
                
                <!-- Receipt body -->
                <div class="col border-bottom px-0 py-2 small">
                    <div class="row">
                        <div class="col-auto">2</div>
                        <div class="col text-break">Bier tap</div>
                        <div class="col-auto">€ 6,00</div>
                    </div>
                    <div class="row">
                        <div class="col-auto">1</div>
                        <div class="col text-break">Appelgebak<p class="text-muted mb-0">+ slagroom</p></div>
                        <div class="col-auto">€ 4,50</div>
                    </div>
                    <div class="row">
                        <div class="col-auto">3</div>
                        <div class="col text-break">Bittergarnituur</div>
                        <div class="col-auto">€ 15,00</div>
                    </div>
                </div>

                <!-- Receipt footer -->
                <div class="col px-0 mt-2">
                    <div class="row">
                        <div class="col"><strong><?php _e('Totaal', 'menukaart');?></strong></div>
                        <div class="col-auto"><strong>€ 25,50</strong></div>
                    </div>
                    <div class="row small">
                        <div class="col"><?php _e('BTW hoog', 'menukaart');?></div>
                        <div class="col-auto">€ 1,26</div>
                    </div>
                    <div class="row small">
                        <div class="col"><?php _e('BTW laag', 'menukaart');?></div>
                        <div class="col-auto">€ 1,75</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col text-center mt-3">
                    <button id="request-payment" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#payModal"><?php _e('Rekening betalen', 'menukaart');?></button>
                </div>
            </div>
        </div>
    <?php endif;?>

</div>

<?php
// Load Footer
require get_stylesheet_directory() . '/assets/footer.php';