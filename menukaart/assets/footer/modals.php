<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;?>

<!-- Menu overview Modal -->
<div class="modal fade" id="menuItemModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body"></div>
            <?php if(get_option('printnode_api') && is_active_customer()) :?>
                <div class="modal-footer justify-content-center"></div>
            <?php endif;?>
        </div>
    </div>
</div>

<?php
if(get_option('printnode_api') && is_active_customer()) :?>
    <!-- Payment Methods Modal -->
    <div class="modal fade" id="payModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <p class="text-center text-muted"><?php _e('Kies een betaalmethode', 'menukaart');?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-3">
                            <button id="request-qr" class="btn btn-lg btn-light w-100 p-4"><?php _e('Pin of Contant', 'menukaart');?></button>
                        </div>
                    </div>
                    <?php
                    // If Mollie API key
                    if(get_option('mollie_api')) :?>
                        <div class="row">
                            <div class="col mb-3">
                                <form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post">
                                    <input type="hidden" name="payment-method" value="bank">
                                    <button type="submit" class="btn btn-lg btn-light w-100 p-4"><?php _e('Internetbankieren', 'menukaart');?></button>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-center">
                                <?php
                                // Get the available payment methods
                                if(!is_wp_error($paymentMethods)) : foreach($paymentMethods as $method) :?>
                                    <img class="" src="<?php echo $method->image->svg;?>" alt="<?php echo $method->description;?>">
                                <?php endforeach; endif;?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>

    <!-- Payment QR Modal -->
    <div class="modal fade" id="paymentQR" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <p class="text-center text-muted mb-0"><?php _e('Laat de QR-code door een medewerker scannen', 'menukaart');?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-center">
                            <img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=<?php echo urlencode(home_url('/?t=' . json_decode(stripslashes($_COOKIE['qr-scan']))->table));?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;

// Table alert modals
if(isset($alertStatus)) :?>
    <!-- Alert Modal -->
    <div class="modal fade<?php if($alertStatus == 'registered') echo ' registered';?>" id="alert" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-justify">
                    <?php if($alertStatus == 'success') :?>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                            <circle class="path circle" fill="none" stroke="#28a745" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                            <polyline class="path check" fill="none" stroke="#28a745" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                        </svg>
                        <p class="text-success text-center mt-3 lead font-weight-bold"><?php _e('Succesvol geregistreerd!', 'menukaart');?></p>
                    <?php endif;?>
                    <?php if($alertStatus == 'occupied') :?>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                            <circle class="path circle" fill="none" stroke="#dc3545" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                            <line class="path line" fill="none" stroke="#dc3545" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                            <line class="path line" fill="none" stroke="#dc3545" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                        </svg>
                        <p class="text-danger text-center mt-3 lead font-weight-bold"><?php _e('Helaas, deze tafel is al bezet', 'menukaart');?></p>
                    <?php endif;?>
                    <?php if($alertStatus == 'registered') :?>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                            <circle class="path circle" fill="none" stroke="#ffc107" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                            <line class="path line" fill="none" stroke="#ffc107" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="65.1" y1="37.9" x2="65.1" y2="77.3"/>
                            <line class="path line" fill="none" stroke="#ffc107" stroke-width="9" stroke-linecap="round" stroke-miterlimit="10" x1="65.1" y1="92.3" x2="65.1" y2="92.3"/>
                        </svg>
                        <p class="text-warning text-center my-3 lead font-weight-bold"><?php _e('Je zit al aan een tafel', 'menukaart');?></p>
                        <form class="text-center" action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post">
                            <input type="hidden" name="switch-tables" value="<?php echo $switchTable;?>">
                            <button id="change-table" type="submit" class="btn btn-lg btn-primary mt-2"><?php _e('Wissel van tafel', 'menukaart');?></button>
                        </form>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>