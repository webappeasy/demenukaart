<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;
?>

                </div>
            </div>
        </main>
        <footer class="fixed-bottom border-top">
            <div class="nav btn-group btn-group-lg w-100" role="tablist">
                <a class="btn btn-light text-dark rounded-0 active p-3" data-toggle="tab" id="menu-overview-tab" href="#menu-overview" role="tab" aria-controls="menu-overview-content" aria-selected="true"><i class="fas fa-utensils fa-fw fa-lg"></i></a>
                <?php if(get_option('printnode_api') && is_active_customer()) :?>
                    <a class="btn btn-light text-dark rounded-0 p-3" data-toggle="tab" id="order-overview-tab" href="#order-overview" role="tab" aria-controls="order-overview-content" aria-selected="false">
                        <i class="fas fa-concierge-bell fa-fw fa-lg"></i>
                        <?php
                        if(isset($_COOKIE['current-order'])) :
                            $amountOrder = 0;
                            foreach(json_decode(stripslashes($_COOKIE['current-order']), true) as $itemOrder) {
                                $amountOrder = $amountOrder + $itemOrder['amount'];
                            }?>
                            <span class="badge badge-pill badge-danger position-absolute order"><?php echo $amountOrder;?></span>
                        <?php endif;?>
                    </a>
                    <a class="btn btn-light text-dark rounded-0 p-3" data-toggle="tab" id="receipt-overview-tab" href="#receipt-overview" role="tab" aria-controls="receipt-overview-content" aria-selected="false"><i class="fas fa-receipt fa-fw fa-lg"></i></a>
                <?php endif;?>
                <a class="btn btn-light text-dark rounded-0 p-3 dropup" id="btnMore" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h fa-fw fa-lg"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnMore">
                    <?php if(is_user_logged_in()) :?>
                        <a class="dropdown-item py-3" href="<?php echo admin_url();?>"><i class="fas fa-tachometer-alt fa-fw mr-3"></i><?php _e('Dashboard', 'menukaart');?></a>
                    <?php endif;?>
                    <?php if(get_option('printnode_api')) :?>
                        <a id="qrcode" class="dropdown-item py-3 d-none"><i class="fas fa-qrcode fa-fw mr-3"></i><?php _e('QR-Scanner', 'menukaart');?></a>
                    <?php endif;?>
                    <a class="dropdown-item py-3" href="#"><i class="fas fa-unlock-alt fa-fw mr-3"></i><?php _e('Privacyverklaring', 'menukaart');?></a>
                </div>
            </div>
        </footer>
        <?php

        // Load footer modals
        require get_stylesheet_directory() . '/assets/footer/modals.php';

        // Load WP footer
        wp_footer();?>
    </body>
</html>