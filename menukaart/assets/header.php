<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Load table setter
require get_stylesheet_directory() . '/assets/header/table-setter.php';

// Load payment methods
require get_stylesheet_directory() . '/assets/header/payment-methods.php';
?>

<!DOCTYPE html>
<html class="h-100" <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="application-name" content="<?php echo get_bloginfo('name');?>">
    <meta name="apple-mobile-web-app-title" content="<?php echo get_bloginfo('name');?>">
    <link rel="apple-touch-startup-image" href="<?php echo get_theme_file_uri('/img/splash/launch-640x1136.png');?>" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="<?php echo get_theme_file_uri('/img/splash/launch-750x1294.png');?>" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="<?php echo get_theme_file_uri('/img/splash/launch-1242x2148.png');?>" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="<?php echo get_theme_file_uri('/img/splash/launch-1125x2436.png');?>" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="<?php echo get_theme_file_uri('/img/splash/launch-1536x2048.png');?>" media="(min-device-width: 768px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="<?php echo get_theme_file_uri('/img/splash/launch-1668x2224.png');?>" media="(min-device-width: 834px) and (max-device-width: 834px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="<?php echo get_theme_file_uri('/img/splash/launch-2048x2732.png');?>" media="(min-device-width: 1024px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="icon" href="<?php echo get_theme_file_uri('/img/favicon.svg');?>" sizes="32x32">
    <link rel="icon" href="<?php echo get_theme_file_uri('/img/favicon.svg');?>" sizes="192x192">
    <link rel="apple-touch-icon" href="<?php echo get_theme_file_uri('/img/favicon.png');?>">
    <meta name="msapplication-TileImage" content="<?php echo get_theme_file_uri('/img/favicon.png');?>">
    <title><?php _e('Online menukaart', 'menukaart');?> | <?php echo get_bloginfo('name');?></title>
    <?php wp_head();?>
</head>
<body <?php body_class(array('d-flex', 'flex-column'));?>>
    <!-- QR Scanner -->
    <div class="scan-container d-none">
        <video id="scan-screen" playsinline></video>
        <canvas id="canvas"></canvas>
        <button id="close-scanner" class="btn btn-lg btn-primary w-auto"><?php _e('Sluit scanner', 'menukaart');?></button>
    </div>

    <header class="fixed-top">
        <!-- Navbar -->
        <nav class="navbar bg-dark navbar-dark">

            <!-- Brand -->
            <div class="navbar-brand m-0"><?php echo get_bloginfo('name');?></div>

            <!-- Language Switcher -->
            <?php do_action('wpml_add_language_selector');?>
            
        </nav>
        
    </header>
    
    <main role="main" class="container-fluid flex-shrink-0">
        <div class="row">
           <div class="container">