<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// If Printnode is set
if(get_option('printnode_api')) {

    // If table switch requested
    if(isset($_POST['switch-tables']) && is_active_customer()) {

        // Get the table post
        $table = get_post($_POST['switch-tables']);

        // Check if post is table posttype
        if($table->post_type == 'tables') {

            // If table is free
            if($table->status != 'occupied') {

                // Get current table
                $currentTable = get_post(json_decode(stripslashes($_COOKIE['qr-scan']))->table);

                // Set new table status
                $uniqID = uniqid();
                $tableStatus = wp_update_post(array(
                    'ID' => $_POST['switch-tables'],
                    'meta_input' => array(
                        'status' => 'occupied',
                        'customerID' => $uniqID
                    )
                ));

                // If update throws error
                if(is_wp_error($tableStatus)) {
                    // Modal error
                    setcookie('alert', 'error');
                    exit;
                }

                // Delete current table data
                delete_post_meta($currentTable->ID, 'status');
                delete_post_meta($currentTable->ID, 'customerID');

                // Set cookie and redirect
                setcookie('qr-scan', json_encode(array('customerID' => $uniqID, 'table' => $_POST['switch-tables'])), time() + 86400, '/', $_SERVER['HTTP_HOST'], true);
                setcookie('alert', 'success');
                wp_redirect(home_url('/'));
                exit;
            }

            // Modal occupied
            setcookie('alert', 'occupied');
            wp_redirect(home_url('/'));
            exit;
        }
    }

    // If QR-code is scanned
    if(isset($_GET['t'])) {

        // Get the table post
        $table = get_post($_GET['t']);

        // Check if post is table posttype and status is published
        if($table->post_type == 'tables' && $table->post_status == 'publish') {

            // If table is free
            if($table->status != 'occupied') {

                // If customer is allready active
                if(is_active_customer()) {

                    // If other table is requested
                    setcookie('alert', 'registered');
                    setcookie('switch-table', $_GET['t']);
                    wp_redirect(home_url('/'));
                    exit;
                }

                // Set new table status
                $uniqID = uniqid();
                $tableStatus = wp_update_post(array(
                    'ID' => $_GET['t'],
                    'meta_input' => array(
                        'status' => 'occupied',
                        'customerID' => $uniqID
                    )
                ));

                // If update throws error
                if(is_wp_error($tableStatus)) {
                    // Modal error
                    setcookie('alert', 'error');
                    wp_redirect(home_url('/'));
                    exit;
                }

                // Set cookie and redirect
                setcookie('qr-scan', json_encode(array('customerID' => $uniqID, 'table' => $_GET['t'])), time() + /*86400*/604800, '/' . pathinfo(home_url())['basename'] . '/', $_SERVER['HTTP_HOST'], true);
                setcookie('alert', 'success');
                wp_redirect(home_url('/'));
                exit;
            }

            // If customer is allready active
            if(is_active_customer()) {

                // If still same table, redirect
                if(json_decode(stripslashes($_COOKIE['qr-scan']))->table == $_GET['t']) {
                    wp_redirect(home_url('/'));
                    exit;
                }
            }

            // Modal occupied
            setcookie('alert', 'occupied');
            wp_redirect(home_url('/'));
            exit;
        }

        // If parameter is not a valid table, redirect
        wp_redirect(home_url('/'));
        exit;
    }
}

// Unset alert cookie after it is set
if(isset($_COOKIE['alert'])) {
    $alertStatus = $_COOKIE['alert'];
    setcookie('alert', '');
}

// Unset switch cookie after it is set
if(isset($_COOKIE['switch-table'])) {
    $switchTable = $_COOKIE['switch-table'];
    setcookie('switch-table', '');
}