<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

if(is_active_customer()) {

    // If customer wants to pay
    if(isset($_POST['payment-method'])) {

        // If customer wants to pay by bank
        if($_POST['payment-method'] == 'bank') {
            $paymentRequest = wp_safe_remote_post('https://api.mollie.com/v2/payments?include=details.qrCode', array(
                'method' => 'POST',
                'headers' => array(
                    'Authorization' => 'Bearer ' . get_option('mollie_api')
                ),
                'body' => array(
                    'amount' => array(
                        'currency' => 'EUR',
                        'value' => "10.00"
                    ),
                    'description' => 'Test order #' . time(),
                    'redirectUrl' => home_url('/?payment=success'),
                    'locale' => $_SERVER['HTTP_ACCEPT_LANGUAGE']
                )
            ));

            // If response 200
            if(!is_wp_error($paymentRequest)) {

                // Convert string response to array of methods
                $paymentRequest = json_decode($paymentRequest['body']);

                // Redirect customer to checkout page
                wp_redirect($paymentRequest->_links->checkout->href);
                exit;
            }
        }
    }

    // If Mollie API key
    if(get_option('mollie_api')) {
        // Get payment methods from Mollie
        $paymentMethods = wp_safe_remote_post('https://api.mollie.com/v2/methods', array(
            'method' => 'GET',
            'headers' => array(
                'Authorization' => 'Bearer ' . get_option('mollie_api'),
                'Content-Type' => 'application/json'
            ),
            'body' => array(
                'includeWallets' => 'applepay',
                'locale' => $_SERVER['HTTP_ACCEPT_LANGUAGE']
            )
        ));

        // If response 200
        if(!is_wp_error($paymentMethods)) {

            // Convert string response to array of methods
            $paymentMethods = json_decode($paymentMethods['body'])->_embedded->methods;
        }
    }
}