<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Function to check if user is an active customer
function is_active_customer() {
    if (isset($_COOKIE['qr-scan'])) {
        $result = (json_decode(stripslashes($_COOKIE['qr-scan']))->customerID == get_post_meta(json_decode(stripslashes($_COOKIE['qr-scan']))->table, 'customerID', true)) ? true : false;
        return $result;
    }

    return false;
}

// On activation theme
add_action('after_setup_theme', function() {
    // Load the translation files
    load_theme_textdomain('menukaart', get_template_directory() . '/languages');

    // Custom Logo
    add_theme_support('custom-logo');

    // Thumbnails
    add_theme_support('post-thumbnails');

    // Custom Background
    add_theme_support('custom-background');

    // Set locale timezone to Amsterdam
    update_option('timezone_string', 'Europe/Amsterdam');
});

// Load Styles and Scripts
require get_stylesheet_directory() . '/inc/styles.php';
require get_stylesheet_directory() . '/inc/scripts.php';

// Load Posttypes
require get_stylesheet_directory() . '/inc/posttypes.php';

// Load Roles
require get_stylesheet_directory() . '/inc/roles.php';

// Load Admin Menus
require get_stylesheet_directory() . '/inc/admin-menus.php';

// Load Custom routes
require get_stylesheet_directory() . '/inc/routes.php';