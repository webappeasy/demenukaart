<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Create API's for Menu
add_action('rest_api_init', 'menu_api');

function menu_api() {
    register_rest_route('menu/v1', 'overview', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'get_products'
    ));
}

function get_products($request) {
    
    // Check if request is made from the site itself
    if(wp_verify_nonce($request->get_header('X-WP-Nonce'), 'wp_rest')) {
        $menuItems = array();

        // Get the active menu
        $activeMenu = apply_filters('wpml_object_id', get_option('current_menu'), 'menu', TRUE );

        // Get categories
        $categories = get_field('categories', $activeMenu);

        if($categories) {
            foreach($categories as $category) {

                $products = array();

                // Get all the products for the current category
                if($category['category_items']) {
                    foreach($category['category_items'] as $product) {

                        $product = $product['product'];
                        $units = array();
                        $unitPrices = array();
                        $allergens = array();
                        $choices = array();

                        // Get all the units for the current product
                        if(get_field('product_units', $product->ID)) {
                            foreach(get_field('product_units', $product->ID) as $unit) {
                                array_push($units, array(
                                    'price' => floatval($unit['price']),
                                    'slug' => sanitize_title($unit['unit']),
                                    'unit' => $unit['unit'],
                                ));

                                array_push($unitPrices, floatval($unit['price']));
                            }
                        }

                        // Get all the allergens for the current product
                        if(get_field('product_allergens', $product->ID)) {
                            foreach(get_field('product_allergens', $product->ID) as $allergen) {
                                array_push($allergens, array(
                                    'icon' => get_theme_file_uri('img/allergens/' . $allergen['value'] . '.svg'),
                                    'text' => $allergen['label'],
                                ));
                            }
                        }

                        // Get all the choices for the current product
                        if(get_field('product_choices', $product->ID)) {
                            foreach(get_field('product_choices', $product->ID) as $choice) {
                                array_push($choices, array(
                                    'name' => $choice->name,
                                    'slug' => $choice->slug,
                                    'price' => floatval(get_field('price', 'product-choices_' . $choice->term_id))
                                ));
                            }
                        }

                        array_push($products, array(
                            'id' => $product->ID,
                            'name' => $product->post_title,
                            'description' => $product->product_description,
                            'image' => get_the_post_thumbnail_url($product->ID, 'medium'),
                            'min_price_text' => (count($unitPrices) > 1) ? __('v.a.', 'menukaart') : '',
                            'min_price' => min($unitPrices),
                            'options_title' => __('Opties', 'menukaart'),
                            'options' => $units,
                            'allergens_title' => __('Allergenen', 'menukaart'),
                            'allergens_content' => $allergens,
                            'choices_title' => __('Extra keuzes', 'menukaart'),
                            'choices_content' => $choices
                        ));
                    }
                }

                // Push to menu items array
                array_push($menuItems, array(
                    'name' => $category['category_name'],
                    'slug' => sanitize_title($category['category_name']),
                    'info' => $category['category_info'],
                    'items' => $products
                ));
            }
        }

        return $menuItems;
    } else {
        die("You don't have permissions to do that.");
    }
}