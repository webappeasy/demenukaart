<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Delete other roles
function delete_roles() {
    remove_role('contributor');
    remove_role('author');
    remove_role('editor');
    remove_role('wpseo_editor');
    remove_role('wpseo_manager');
}
add_action('admin_init', 'delete_roles');

// Add admin role capabilities
function add_roles() {

    // Admin
    get_role('administrator')->add_cap('delete_menus');
    get_role('administrator')->add_cap('delete_others_menus');
    get_role('administrator')->add_cap('delete_published_menus');
    get_role('administrator')->add_cap('edit_menus');
    get_role('administrator')->add_cap('edit_others_menus');
    get_role('administrator')->add_cap('edit_published_menus');
    get_role('administrator')->add_cap('publish_menus');

    get_role('administrator')->add_cap('delete_products');
    get_role('administrator')->add_cap('delete_others_products');
    get_role('administrator')->add_cap('delete_published_products');
    get_role('administrator')->add_cap('edit_products');
    get_role('administrator')->add_cap('edit_others_products');
    get_role('administrator')->add_cap('edit_published_products');
    get_role('administrator')->add_cap('publish_products');

    get_role('administrator')->add_cap('delete_tables');
    get_role('administrator')->add_cap('delete_others_tables');
    get_role('administrator')->add_cap('delete_published_tables');
    get_role('administrator')->add_cap('edit_tables');
    get_role('administrator')->add_cap('edit_others_tables');
    get_role('administrator')->add_cap('edit_published_tables');
    get_role('administrator')->add_cap('publish_tables');

    if(!get_role('management')) {
        add_role('management', __('Management', 'menukaart'));
    } else {

        // Management
        get_role('management')->add_cap('delete_menus');
        get_role('management')->add_cap('delete_others_menus');
        get_role('management')->add_cap('delete_published_menus');
        get_role('management')->add_cap('edit_menus');
        get_role('management')->add_cap('edit_others_menus');
        get_role('management')->add_cap('edit_published_menus');
        get_role('management')->add_cap('publish_menus');

        get_role('management')->add_cap('delete_products');
        get_role('management')->add_cap('delete_others_products');
        get_role('management')->add_cap('delete_published_products');
        get_role('management')->add_cap('edit_products');
        get_role('management')->add_cap('edit_others_products');
        get_role('management')->add_cap('edit_published_products');
        get_role('management')->add_cap('publish_products');

        get_role('management')->add_cap('delete_tables');
        get_role('management')->add_cap('delete_others_tables');
        get_role('management')->add_cap('delete_published_tables');
        get_role('management')->add_cap('edit_tables');
        get_role('management')->add_cap('edit_others_tables');
        get_role('management')->add_cap('edit_published_tables');
        get_role('management')->add_cap('publish_tables');

        get_role('management')->add_cap('create_users');
        get_role('management')->add_cap('delete_users');
        get_role('management')->add_cap('edit_users');
        get_role('management')->add_cap('list_users');
        get_role('management')->add_cap('manage_network_users');
        get_role('management')->add_cap('promote_users');
        get_role('management')->add_cap('remove_users');

        get_role('management')->add_cap('manage_product-choices');
        get_role('management')->add_cap('edit_product-choices');
        get_role('management')->add_cap('delete_product-choices');
        get_role('management')->add_cap('assign_product-choices');

        get_role('management')->add_cap('wpml_manage_taxonomy_translation');
        get_role('management')->add_cap('read');
        get_role('management')->add_cap('translate');
        get_role('management')->add_cap('upload_files');
    }

    // Add translation permissions
    $langIndex = 0;
    $extraLanguages = array();
    if(apply_filters('wpml_active_languages', NULL)) {
        foreach(apply_filters('wpml_active_languages', NULL) as $language) {
            if($langIndex) $extraLanguages[$language['code']] = 1;
            $langIndex++;
        }

        if(current_user_can('translate')) {
            update_user_option(get_current_user_id(), 'language_pairs', array(apply_filters('wpml_default_language', NULL) => $extraLanguages), false);
        }
    }
}
add_action('admin_init', 'add_roles');

// Hide superadmin role from dropdown
add_filter('editable_roles', function($editable_roles) {
    unset($editable_roles['super_admin']);
    unset($editable_roles['administrator']);
    return $editable_roles;
});

// Remove superadmin from user list
add_filter('views_users', 'hide_superadmin');
function hide_superadmin($views){
    if(!is_super_admin()){
        unset($views['super_admin']);
        unset($views['administrator']);
    }
    return $views;
}

add_action('pre_user_query', 'user_query');
function user_query($user_search) {
    if(!is_super_admin()){
        global $wpdb;
        $user_search->query_where = str_replace('WHERE 1=1', "WHERE 1=1 AND {$wpdb->users}.ID<>1", $user_search->query_where);
    }
}