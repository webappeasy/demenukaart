<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Load global options for wp-admin
require get_stylesheet_directory() . '/inc/admin-options/global.php';

// Load custom fields
require get_stylesheet_directory() . '/inc/admin-options/custom-fields.php';

// Load Printnode options for settings page
require get_stylesheet_directory() . '/inc/admin-options/printnode.php';

// Load mollie options for settings page if printnode is set
if(get_option('printnode_api')) {
    require get_stylesheet_directory() . '/inc/admin-options/mollie.php';
}

add_action( 'admin_menu', 'extra_post_info_menu' );  
function extra_post_info_menu(){    
    $page_title = 'Product overzicht';   
    $menu_title = 'Overzicht';   
    $capability = 'manage_options';   
    $menu_slug  = 'extra-post-info';   
    $function   = 'extra_post_info_page';   
    $icon_url   = 'dashicons-analytics';   
    $position   = 4;    
    add_menu_page( 
        $page_title,                  
        $menu_title,                   
        $capability,                   
        $menu_slug,                   
        $function,                   
        $icon_url,                   
        $position 
    ); 
}
if( !function_exists("extra_post_info_page") ) { 
    function extra_post_info_page(){ 
        ?>
        <div class="wrap">
            <h1>Overzicht</h1> 
            <div class="d-flex justify-content-start">

                <select id="filter-field">
                    <option value="name"> Naam</option>
                    <option value="buy">inkoop</option>
                    <option value="sell">verkoop</option>
                    <option value="status" selected>BTW-Status</option>
                    <option value="qty">Hoeveelheid</option>
                    <option value="excl">Totaal excl.</option>
                    <option value="btw">BTW</option>
                    <option value="incl">Totaal incl.</option>
                </select>

                <select id="filter-type">
                    <option value="like">Bevat</option>
                    <option value="=">Exact</option>
                    <option value="<">Kleiner dan</option>
                    <option value=">">Groter dan</option>
                    <option value="!=">Is niet</option>
                    
                </select>

                <input id="filter-value" type="text" placeholder="Waarde">

                <button class="button" id="download-xlsx">Download XLSX</button>
                <button class="button" id="download-pdf">Download PDF</button>

            </div>
            
            <div id="example-table"></div>
        </div>
        <?php
        
    } 
} 
?>
