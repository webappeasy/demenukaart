<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Load styles front-end
function enqueue_styles() {
    wp_enqueue_style('bootstrap-css', get_theme_file_uri('/assets/bootstrap/css/bootstrap.min.css'));
    wp_enqueue_style('animate-css', get_theme_file_uri('/assets/animate/animate.min.css'));
    wp_enqueue_style('fontawesome-css', 'https://use.fontawesome.com/releases/v5.13.0/css/all.css');
    wp_enqueue_style('theme-css', get_theme_file_uri('/css/theme.min.css'), array(), time());
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

// Load styles admin
function enqueue_admin_styles($hook) {
    //wp_enqueue_style('bootstrap-css', get_theme_file_uri('/assets/bootstrap/css/bootstrap.min.css'));
    wp_enqueue_style('tabulator-css', get_theme_file_uri('/assets/tabulator/css/tabulator.min.css'));
    wp_enqueue_style('admin-css', get_theme_file_uri('/css/admin.min.css'), array(), time());
}
add_action('admin_enqueue_scripts', 'enqueue_admin_styles');