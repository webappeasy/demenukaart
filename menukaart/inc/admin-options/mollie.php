<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Add Mollie options section to General Settings page
add_action('admin_init', function() {
	add_settings_section(
		'mollie_options',
		__('Mollie instellingen', 'menukaart'),
		'mollie_options_cb',
		'general'
	);

	add_settings_field(
		'mollie_api',
		__('Mollie API-sleutel', 'menukaart'),
		'mollie_api_cb',
		'general',
		'mollie_options',
		array('mollie_api')
	);

	register_setting('general', 'mollie_api', 'esc_attr');
});

function mollie_options_cb() {
	printf(__('Om internetbankieren via %s te activeren.', 'menukaart'), '<a href="https://www.mollie.com" target="_blank">Mollie</a>');
}

function mollie_api_cb($args) {
	$option = get_option($args[0]);
	echo '<input type="password" class="regular-text" id="' . $args[0] . '" name="' . $args[0] . '" value="' . $option . '">';
}