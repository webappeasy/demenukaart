<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Require hCaptcha for login
require get_stylesheet_directory() . '/inc/admin-options/hcaptcha/hcaptcha-login.php';

// Hide adminbar from front-end
if (!is_admin()) {
    add_filter('show_admin_bar', '__return_false');
}

// Remove items from admin bar
function remove_from_admin_bar($wp_admin_bar) {
    $wp_admin_bar->remove_node('updates');
    $wp_admin_bar->remove_node('comments');
    $wp_admin_bar->remove_node('new-content');
    $wp_admin_bar->remove_node('wp-logo');
    if(!current_user_can('manage_options')) $wp_admin_bar->remove_node('my-sites');
}
add_action('admin_bar_menu', 'remove_from_admin_bar', 999);

// Remove items from admin menu
function post_remove () {
    remove_menu_page('edit-comments.php');
    remove_menu_page('edit.php');
    remove_menu_page('upload.php');
    remove_menu_page('edit.php?post_type=page');
}
add_action('admin_menu', 'post_remove');

// Metaboxes from dashboard
function admin_widgets(){
    
    // Remove widgets
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
	remove_meta_box('dashboard_activity', 'dashboard', 'normal');
    remove_meta_box('tinypng_dashboard_widget', 'dashboard', 'normal');
    remove_meta_box('dashboard_site_health', 'dashboard', 'normal');

    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    remove_meta_box('dashboard_primary', 'dashboard', 'side');

    remove_action('welcome_panel', 'wp_welcome_panel');

    // Add widgets
    add_meta_box('qr-menu', __('QR-Code menukaart', 'menukaart'), 'qr_dashboard_callback', 'dashboard', 'column3');
}
add_action('wp_dashboard_setup', 'admin_widgets', 20);

// Metaboxes global
add_action('admin_head', function($post_type) {
    remove_meta_box('icl_div', $post_type, 'side');

    remove_meta_box('slugdiv', $post_type, 'normal');
    remove_meta_box('icl_div_config', $post_type, 'normal');
}, 99, 1);

function qr_dashboard_callback($post) {
    echo '<center><img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=' . urlencode(home_url()) . '" title="' . __('Download QR-Code', 'menukaart') . '" /></center>';
    echo '<center><a href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=' . urlencode(home_url()) . '" target="_blank">' . __('Hogere resolutie', 'menukaart') . '</a></center>';
}

// Change WP admin footer
function left_admin_footer_text_output($text) {
    return '';
}
add_filter('admin_footer_text', 'left_admin_footer_text_output');
 
function right_admin_footer_text_output($text) {
    return '<a href="mailto:support@apphoreca.nl">' . __('Contact support', 'menukaart') . '</a>';
}
add_filter('update_footer', 'right_admin_footer_text_output', 11);

// Change admin title
function change_admin_title($admin_title, $title) {
    return $title . ' | ' . get_bloginfo('name');
}
add_filter('admin_title', 'change_admin_title', 10, 2);

// Change login header URL
add_filter('login_headerurl', 'loginHeaderUrl');
function loginHeaderUrl() {
    return esc_url(home_url());
}

// Change header title
add_filter('login_title', 'custom_login_title');
function custom_login_title($login_title) {
    $login_title = __('Log In') . ' | ' . get_bloginfo('name');
	return $login_title;
}

// Login style sheet
add_action('login_enqueue_scripts', 'loginScripts');
function loginScripts() {
    wp_enqueue_style('login-style', get_template_directory_uri() . '/css/login.min.css');
}