<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Add hCaptcha options section to Network Settings page
add_filter('wpmu_options' , 'show_hcaptcha_network_settings');
add_action('update_wpmu_options', 'save_hcaptcha_network_settings');

function save_hcaptcha_network_settings() {

  // hCaptcha secret key
  if(isset($_POST['hcaptcha_secret_key']))
    update_site_option('hcaptcha_secret_key', $_POST['hcaptcha_secret_key']);

  // hCaptcha site key
  if(isset($_POST['hcaptcha_site_key']))
    update_site_option('hcaptcha_site_key', $_POST['hcaptcha_site_key']);
}

function show_hcaptcha_network_settings() {
    $hcaptchaSecret = get_site_option('hcaptcha_secret_key'); 
    $hcaptchaSite = get_site_option('hcaptcha_site_key'); 
    ?>
    <h3><?php _e('hCaptcha netwerk opties', 'menukaart'); ?></h3>
    <p><?php _e('Een nieuwe manier om geld te verdienen via uw siteverkeer.', 'menukaart');?></p>
    <table class="form-table">
        <tbody>
            <tr>
                <th scope="row"><?php _e('hCaptcha geheime sleutel', 'menukaart'); ?></th>
                <td>
                    <label>
                        <input type="password" class="regular-text" name="hcaptcha_secret_key" value="<?php echo $hcaptchaSecret;?>"/> 
                    </label>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php _e('hCaptcha site sleutel', 'menukaart'); ?></th>
                <td>
                    <label>
                        <input type="text" class="regular-text" name="hcaptcha_site_key" value="<?php echo $hcaptchaSite;?>"/> 
                    </label>
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

// Add hCaptcha to login, register and password forms
if(get_site_option('hcaptcha_secret_key') && get_site_option('hcaptcha_site_key')) {
    
    // Load hCaptcha script
    add_action('login_enqueue_scripts', function() {
        wp_enqueue_script('jquery');
        wp_enqueue_script('hcaptcha-script', 'https://hcaptcha.com/1/api.js', array(), false, true);
        wp_enqueue_script('hcaptcha-token', get_theme_file_uri('/inc/admin-options/hcaptcha/hcaptcha.min.js'), array(), false, true);
    });

    // Add to login form
    add_filter('login_form', function () {
        $hcaptcha_site_key = get_site_option('hcaptcha_site_key' );
        echo '<div class="h-captcha" data-sitekey="' . $hcaptcha_site_key . '" data-size="invisible" data-callback="onSubmitLogin"></div>';
    });

    // Add to register form
    add_filter('register_form', function () {
        $hcaptcha_site_key = get_site_option('hcaptcha_site_key' );
        echo '<div class="h-captcha" data-sitekey="' . $hcaptcha_site_key . '" data-size="invisible" data-callback="onSubmitRegister"></div>';
    });

    // Add to lost password form
    add_filter('lostpassword_form', function () {
        $hcaptcha_site_key = get_site_option('hcaptcha_site_key' );
        echo '<div class="h-captcha" data-sitekey="' . $hcaptcha_site_key . '" data-size="invisible" data-callback="onSubmitPassword"></div>';
    });

    // hCaptcha check on login
    add_filter('wp_authenticate_user', function ($user, $password) {
        if (isset($_POST['h-captcha-response'])) {
            $get_hcaptcha_response = htmlspecialchars(sanitize_text_field($_POST['h-captcha-response']));
            $hcaptcha_secret_key = get_site_option('hcaptcha_secret_key');
            
            $response = wp_remote_get('https://hcaptcha.com/siteverify?secret=' . $hcaptcha_secret_key . '&response=' . $get_hcaptcha_response);
            $response = json_decode($response['body'], true);
            if (true == $response['success']) {
                return $user;
            } else {
                return new WP_Error('Captcha Invalid', __('<strong>FOUT</strong>: Ongeldige Captcha', 'menukaart'));
            } 
        } else {
            return new WP_Error('Captcha Invalid', __('<strong>FOUT</strong>: Ongeldige Captcha', 'menukaart'));
        }   
    }, 10, 2);

    // hCaptcha check on register
    add_filter('registration_errors', function ($errors, $sanitized_user_login, $user_email) {
        if (isset($_POST['h-captcha-response'])) {
            $get_hcaptcha_response = htmlspecialchars(sanitize_text_field($_POST['h-captcha-response']));
            $hcaptcha_secret_key = get_site_option('hcaptcha_secret_key');

            $response = wp_remote_get('https://hcaptcha.com/siteverify?secret=' . $hcaptcha_secret_key . '&response=' . $get_hcaptcha_response);
            $response = json_decode($response['body'], true);
            if (true == $response['success']) {
                return $errors;
            } else {
                return new WP_Error('Captcha Invalid', __('<strong>FOUT</strong>: Ongeldige Captcha', 'menukaart'));
            } 
        } else {
            return new WP_Error('Captcha Invalid', __('<strong>FOUT</strong>: Ongeldige Captcha', 'menukaart'));
        }  
    }, 10, 3);

    // hCaptcha check on lost password form
    add_filter('allow_password_reset', function ($true) {
        if (isset($_POST['h-captcha-response'])) {
            $get_hcaptcha_response = htmlspecialchars(sanitize_text_field($_POST['h-captcha-response']));
            $hcaptcha_secret_key = get_site_option('hcaptcha_secret_key');

            $response = wp_remote_get('https://hcaptcha.com/siteverify?secret=' . $hcaptcha_secret_key . '&response=' . $get_hcaptcha_response);
            $response = json_decode($response['body'], true);
            if (true == $response['success']) {
                return $true;
            } else {
                return new WP_Error('Captcha Invalid', __('<strong>FOUT</strong>: Ongeldige Captcha', 'menukaart'));
            } 
        } else {
            if(!isset($_POST['user-password'])) {
                return new WP_Error('Captcha Invalid', __('<strong>FOUT</strong>: Ongeldige Captcha', 'menukaart'));
            } else {
                return $true;
            }
        }   
    });
}