<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Add Printnode options section to General Settings page
add_action('admin_init', function() {
	add_settings_section(
		'printnode_options',
		__('Printnode instellingen', 'menukaart'),
		'printnode_options_cb',
		'general'
	);

	add_settings_field(
		'printnode_api',
		__('Printnode API-sleutel', 'menukaart'),
		'printnode_api_cb',
		'general',
		'printnode_options',
		array('printnode_api')
	);

	register_setting('general', 'printnode_api', 'esc_attr');
});

function printnode_options_cb() {
	printf(__('Om printers via %s te activeren.', 'menukaart'), '<a href="https://www.printnode.com/" target="_blank">PrintNode</a>');
}

function printnode_api_cb($args) {
	$option = get_option($args[0]);

	if($option) {
		$printnodeRequest = wp_safe_remote_post('https://api.printnode.com/whoami', array(
			'method' => 'GET',
			'headers' => array(
				'Authorization' => 'Basic ' . base64_encode($option)
			)
		));

		$response = json_decode($printnodeRequest['body']);

		if(wp_remote_retrieve_response_code($printnodeRequest) == 200) {
			$printnodeStatus = '<span class="badge badge-success">' . ucfirst($response->state) . '</span>';
		} else {
			$printnodeStatus = '<span class="badge badge-danger">' . $response->message . '</span>';
		}
	}

	echo '<input type="password" class="regular-text" id="' . $args[0] . '" name="' . $args[0] . '" value="' . $option . '"> ';
	if(isset($printnodeStatus)) echo $printnodeStatus;
}