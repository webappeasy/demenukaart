<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5ece2aeecc582',
		'title' => 'Keuze prijzen',
		'fields' => array(
			array(
				'key' => 'field_5ece2b01b55b1',
				'label' => 'Verkoopprijs',
				'name' => 'price',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '€',
				'append' => '',
				'min' => 0,
				'max' => '',
				'step' => '',
			),
			array(
				'key' => 'field_5ece3f04dcdbf',
				'label' => 'BTW',
				'name' => 'tax',
				'type' => 'select',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5ece2b01b55b1',
							'operator' => '!=empty',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 1,
				'choices' => array(
					'low' => 'BTW laag',
					'high' => 'BTW Hoog',
				),
				'default_value' => false,
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'return_format' => 'array',
				'ajax' => 0,
				'placeholder' => '',
			),
			array(
				'key' => 'field_5ece4ea42d0d7',
				'label' => 'Inkoopprijs',
				'name' => 'purchase_price',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5ece2b01b55b1',
							'operator' => '!=empty',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '€',
				'append' => '',
				'min' => 0,
				'max' => '',
				'step' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => 'product-choices',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
	
	acf_add_local_field_group(array(
		'key' => 'group_5ece7e6c20774',
		'title' => 'Menukaart indeling',
		'fields' => array(
			array(
				'key' => 'field_5ece7e964b06a',
				'label' => 'Categorieën',
				'name' => 'categories',
				'type' => 'repeater',
				'instructions' => 'Maak categorieën aan en zet de bijhorende producten hier in. De volgorde is aan te passen door te slepen.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 1,
				'collapsed' => '',
				'min' => 1,
				'max' => 0,
				'layout' => 'block',
				'button_label' => 'Nieuwe categorie',
				'sub_fields' => array(
					array(
						'key' => 'field_5ece7f58eb5a5',
						'label' => 'Categorie naam',
						'name' => 'category_name',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'wpml_cf_preferences' => 2,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5ed4bfd06991f',
						'label' => 'Extra informatie',
						'name' => 'category_info',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_5ece7f58eb5a5',
									'operator' => '!=empty',
								),
							),
						),
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => '',
						'wpml_cf_preferences' => 2,
					),
					array(
						'key' => 'field_5ece7fd02a5ed',
						'label' => 'Categorie items',
						'name' => 'category_items',
						'type' => 'repeater',
						'instructions' => 'Voeg producten toe aan deze categorie. De volgorde is aan te passen door te slepen.',
						'required' => 0,
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_5ece7f58eb5a5',
									'operator' => '!=empty',
								),
							),
						),
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'wpml_cf_preferences' => 1,
						'collapsed' => '',
						'min' => 1,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Nieuw product',
						'sub_fields' => array(
							array(
								'key' => 'field_5ece803b2a5ee',
								'label' => 'Product',
								'name' => 'product',
								'type' => 'post_object',
								'wpml_cf_preferences' => 1,
								'instructions' => '',
								'required' => 1,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'post_type' => array(
									0 => 'products',
								),
								'taxonomy' => '',
								'allow_null' => 0,
								'multiple' => 0,
								'return_format' => 'object',
								'ui' => 1,
							),
						),
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'menu',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
	
	acf_add_local_field_group(array(
		'key' => 'group_5ece0341ec668',
		'title' => 'Product informatie',
		'fields' => array(
			array(
				'key' => 'field_5ece034a4f3ec',
				'label' => 'Korte omschrijving',
				'name' => 'product_description',
				'type' => 'text',
				'instructions' => 'Vul een korte omschrijving in van het product',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 2,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5ece17cded5db',
				'label' => 'Allergenen',
				'name' => 'product_allergens',
				'type' => 'select',
				'instructions' => 'Selecteer welke allergenen dit product bevat.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 1,
				'choices' => array(
					'egg' => __('Ei', 'menukaart'),
					'gluten' => __('Gluten', 'menukaart'),
					'lupin' => __('Lupine', 'menukaart'),
					'milk' => __('Melk', 'menukaart'),
					'mustard' => __('Mosterd', 'menukaart'),
					'nuts' => __('Noten', 'menukaart'),
					'peanuts' => __('Pinda\'s', 'menukaart'),
					'shellfish' => __('Schaaldieren', 'menukaart'),
					'celery' => __('Selderij', 'menukaart'),
					'sesame' => __('Sesamzaad', 'menukaart'),
					'soya' => __('Soja', 'menukaart'),
					'fish' => __('Vis', 'menukaart'),
					'molluscs' => __('Weekdieren', 'menukaart'),
					'sulphur' => __('Zwavel', 'menukaart'),
				),
				'default_value' => array(
				),
				'allow_null' => 0,
				'multiple' => 1,
				'ui' => 1,
				'ajax' => 1,
				'return_format' => 'array',
				'placeholder' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'products',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
	
	acf_add_local_field_group(array(
		'key' => 'group_5ece0a8b1b092',
		'title' => 'Prijs informatie',
		'fields' => array(
			array(
				'key' => 'field_5ece06e48dcc5',
				'label' => 'Prijzen',
				'name' => 'product_units',
				'type' => 'repeater',
				'instructions' => 'Vul de prijs in voor het product. Klik op de knop "Extra optie" om meerdere prijzen en eenheden toe te voegen.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 1,
				'collapsed' => '',
				'min' => 1,
				'max' => 0,
				'layout' => 'table',
				'button_label' => 'Extra optie',
				'sub_fields' => array(
					array(
						'key' => 'field_5ece10e7549b3',
						'label' => 'Verkoopprijs',
						'name' => 'price',
						'type' => 'number',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'wpml_cf_preferences' => 1,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '€',
						'append' => '',
						'min' => 0,
						'max' => '',
						'step' => '',
					),
					array(
						'key' => 'field_5ece1100549b4',
						'label' => 'Eenheid',
						'name' => 'unit',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_5ece06e48dcc5',
									'operator' => '>',
									'value' => '1',
								),
							),
						),
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'wpml_cf_preferences' => 2,
						'default_value' => '',
						'placeholder' => 'Bijvoorbeeld: stuks / klein glas',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5ece4df97eba1',
						'label' => 'Inkoopprijs',
						'name' => 'purchase_price',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_5ece10e7549b3',
									'operator' => '!=empty',
								),
							),
							array(
								array(
									'field' => 'field_5ece06e48dcc5',
									'operator' => '>',
									'value' => '1',
								),
							),
						),
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'wpml_cf_preferences' => 1,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '€',
						'append' => '',
						'min' => 0,
						'max' => '',
						'step' => '',
					),
				),
			),
			array(
				'key' => 'field_5ece31e89abc5',
				'label' => 'BTW',
				'name' => 'tax',
				'type' => 'select',
				'instructions' => 'Kies onder welke BTW categorie dit product behoort.',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 1,
				'choices' => array(
					'low' => 'BTW laag',
					'high' => 'BTW hoog',
				),
				'default_value' => false,
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'return_format' => 'array',
				'ajax' => 0,
				'placeholder' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'products',
				),
			),
		),
		'menu_order' => 1,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
	
	acf_add_local_field_group(array(
		'key' => 'group_5ece27474068e',
		'title' => 'Extra keuzes',
		'fields' => array(
			array(
				'key' => 'field_5ece275146a3e',
				'label' => 'Keuzes',
				'name' => 'product_choices',
				'type' => 'taxonomy',
				'instructions' => 'Kies extra keuzes die aan het product toe te voegen zijn.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 1,
				'taxonomy' => 'product-choices',
				'field_type' => 'multi_select',
				'allow_null' => 0,
				'add_term' => 0,
				'save_terms' => 1,
				'load_terms' => 1,
				'return_format' => 'object',
				'multiple' => 0,
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'products',
				),
			),
		),
		'menu_order' => 2,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

	// Filter Page object fields to only show published products
	add_filter('acf/fields/post_object/query', 'products_post_object_query', 10, 3);

	function products_post_object_query($args, $field, $post_id) {
		$args['post_status'] = 'publish';

		return $args;
	}
	
endif;