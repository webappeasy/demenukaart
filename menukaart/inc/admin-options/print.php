<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Set timezone to Europe/Amsterdam
setlocale(LC_ALL, 'nl_NL'); // Set language to Dutch
date_default_timezone_set("Europe/Amsterdam"); // Set timezone to Europe/Amsterdam

// ASCII constants
const ESC = "\x1b";
const GS="\x1d";
const NUL="\x00";
const EURO="\x1B\x74\x13\xD5";
const FONTBIG="\x1D\x21\x11";
const FONTNORMAL="\x1D\x21\x00";
const RIGHTALIGN="\x1B\x61\x32";
const LEFTALIGN="\x1B\x61\x30";
const EMMODEON="\x1B\x21\x30";
const EMMODEOFF="\x1B\x21\x0A\x1B\x45\x0A";
const LINE="\x0A";
const TAB="\x09";
const BACKSPACE="\x08";

// Variables
$restaurantNameRuleOne = "Strandpaviljoen";
$restaurantNameRuleTwo = "Zuid Zuid West";
$restaurantAddress = "Strand Renesse-West 2";
$restaurantPostalCode = "4325 DM Renesse";
$restaurantPhoneNumber = "0111-461568";
$restaurantWebsite = "www.zuid-zuid-west.nl";
$receiptExtraRule1 = "Bedankt voor uw bezoek!"; // First custom line
$receiptExtraRule2 = "Volg ons op Facebook.com/zzwrenesse"; // Second custom line
$receiptExtraRule3 = "Of op Instagram via /zuidzuidwest"; // Third custom line

$tableNumber = "Tafel 101";

// Output the basic receipt
$receipt = ESC."@"; // Reset to defaults
$receipt .= ESC."E".chr(1); // Bold
$receipt .= ESC."a".chr(1); // Centered printing
$receipt .= FONTBIG; // Double font size
$receipt .= $restaurantNameRuleOne . "\n"; // Company
if (!empty($restaurantNameRuleTwo)) { 
    $receipt .= $restaurantNameRuleTwo . "\n";
};
$receipt .= FONTNORMAL;
$receipt .= ESC."d".chr(1); // Blank line
$receipt .= $restaurantAddress . "\n";
$receipt .= $restaurantPostalCode . "\n";
$receipt .= "Tel:" . $restaurantPhoneNumber . "\n";
$receipt .= "Web:" . $restaurantWebsite . "\n";
$receipt .= ESC."a".chr(0); // Escape centered printing
$receipt .= ESC."E".chr(0); // Not Bold
$receipt .= ESC."d".chr(1); // Blank line
$receipt .= "-----------------------------------------------\n";
$receipt .= "1 Carpaccio ossenhaas" . TAB . TAB . TAB . EURO . " 11.50\n"; // Print text
$receipt .= "  1 Extra mayonaise" . TAB . TAB . TAB .EURO. "  0.50\n"; // Print text
$receipt .= "2 Camembert" . TAB . TAB . TAB . "a ".EURO."9.50" . TAB .EURO." 19.00\n"; // Print text
$receipt .= "1 Grote wilde gamba's" . TAB . TAB . TAB .EURO." 11.50\n"; // Print text
$receipt .= "-----------------------------------------------\n";
$receipt .= "Totaal" . TAB . TAB . TAB . TAB . TAB . EURO . " 42.50\n";
$receipt .= "-----------------------------------------------\n";
$receipt .= "BTW Laag (9%)" . TAB . TAB . TAB . TAB . EURO . "  2.40\n";
$receipt .= "BTW Hoog (21%)" . TAB . TAB . TAB . TAB . EURO . "  0.00\n";
$receipt .= "-----------------------------------------------\n";
$receipt .= ESC."d".chr(1); // 1 Blank line
$receipt .= ESC."E".chr(1); // Bold
$receipt .= ESC."a".chr(1); // Centered printing
if (!empty($receiptExtraRule1)) { 
    $receipt .= $receiptExtraRule1 . "\n"; 
};
if (!empty($receiptExtraRule2)) { 
    $receipt .= $receiptExtraRule2 . "\n"; 
};
if (!empty($receiptExtraRule3)) { 
    $receipt .= $receiptExtraRule3 . "\n"; 
};
$receipt .= ESC."a".chr(0); // Escape centered printing
$receipt .= ESC."E".chr(0); // Not Bold
$receipt .= ESC."d".chr(2); // 2 Blank lines
$receipt .= "Datum en tijd: " .strftime('%A %e %b %Y om %H:%M:%S') . "\n";
$receipt .= GS."V\x41".chr(3); // Cut paper


// Output the bar ticket
$barticket = ESC."@"; // Reset to defaults
$barticket .= ESC."d".chr(2); // 2 blank lines
$barticket .= ESC."E".chr(1); // Bold
$barticket .= ESC."a".chr(1); // Centered printing
$barticket .= FONTBIG; // Double font size
$barticket .= $tableNumber; // Table number
$barticket .= ESC."E".chr(0); // Not Bold
$barticket .= ESC."d".chr(2); // Blank line
$barticket .= ESC."a".chr(0); // Escape centered printing
$barticket .= "1 Koffie\n"; // Print product
$barticket .= " + Extra slagroom\n"; // Print extra
$barticket .= "2 Cappuccino\n"; // Print product
$barticket .= "1 Appelgebak\n"; // Print text
$barticket .= ESC."d".chr(2); // 2 Blank lines
$barticket .= FONTNORMAL;
$barticket .= "Datum en tijd: " .strftime('%A %e %b %Y om %H:%M:%S') . "\n";
$barticket .= GS."V\x41".chr(3); // Cut paper


// Output the kitchen ticket
$kitchenTicket = ESC."@"; // Reset to defaults
$kitchenTicket .= ESC."d".chr(2); // 2 blank lines
$kitchenTicket .= ESC."E".chr(1); // Bold
$kitchenTicket .= ESC."a".chr(1); // Centered printing
$kitchenTicket .= FONTBIG; // Double font size
$kitchenTicket .= $tableNumber; // Table number
$kitchenTicket .= ESC."E".chr(0); // Not Bold
$kitchenTicket .= ESC."d".chr(2); // Blank line
$kitchenTicket .= ESC."a".chr(0); // Escape centered printing
$kitchenTicket .= "1 Carpaccio ossenhaas\n"; // Print product
$kitchenTicket .= " + Extra mayonaise\n"; // Print extra
$kitchenTicket .= " + Extra kaas\n"; // Print extra
$kitchenTicket .= ESC."a".chr(1); // Centered printing
$kitchenTicket .= ESC."d".chr(1); // Blank line
$kitchenTicket .= "--- Hoofdgerechten ---\n"; // Main courses
$kitchenTicket .= ESC."d".chr(1); // Blank line
$kitchenTicket .= ESC."a".chr(0); // Escape centered printing
$kitchenTicket .= "2 Camembert\n"; // Print product
$kitchenTicket .= "1 Grote wilde gamba's\n"; // Print text
$kitchenTicket .= ESC."d".chr(2); // 2 Blank lines
$kitchenTicket .= FONTNORMAL;
$kitchenTicket .= "Datum en tijd: " .strftime('%A %e %b %Y om %H:%M:%S') . "\n";
$kitchenTicket .= GS."V\x41".chr(3); // Cut paper

/*
// Receipt in Base64 ?>
<h2>Base64 receipt:</h2>
<?php echo base64_encode($receipt);
// Bar ticket in Base64 ?>
<h2>Base64 bar ticket:</h2>
<?php echo base64_encode($barticket); 
// Kitchen ticket in base64 ?>
<h2>Base64 Kitchen ticket:</h2>
<?php echo base64_encode($kitchenTicket); ?>
*/