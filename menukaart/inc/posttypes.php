<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Create custom posttypes
function create_posttype() {
    // Menu cards
    register_post_type('menu', array(
        'labels' => array(
            'name'          => __('Menukaarten', 'menukaart'), 
            'singular_name' => __('Menukaart', 'menukaart'),
            'add_new'       => __('Menukaart aanmaken', 'menukaart'),
            'add_new_item'  => __('Nieuwe menukaart maken', 'menukaart'),
            'new_item'      => __('Nieuwe menukaart', 'menukaart'),
            'edit_item'     => __('Wijzig menukaart', 'menukaart'),
            'view_item'     => __('Bekijk menukaart', 'menukaart'),
            'all_items'     => __('Alle menukaarten', 'menukaart'),
            'search_items'  => __('Zoek menukaarten', 'menukaart'),
        ),
        'public' => false,
        'show_ui' => true,
        'supports' => array('title'),
        'has_archive' => false,
        'menu_icon' => 'dashicons-list-view',
        'capability_type' => 'menu',
        'map_meta_cap' => true
    ));

    // Products
    register_post_type('products', array(
        'labels' => array(
            'name'          => __('Producten', 'menukaart'), 
            'singular_name' => __('Product', 'menukaart'),
            'add_new'       => __('Product aanmaken', 'menukaart'),
            'add_new_item'  => __('Nieuw product maken', 'menukaart'),
            'new_item'      => __('Nieuw product', 'menukaart'),
            'edit_item'     => __('Wijzig product', 'menukaart'),
            'view_item'     => __('Bekijk product', 'menukaart'),
            'all_items'     => __('Alle producten', 'menukaart'),
            'search_items'  => __('Zoek producten', 'menukaart'),
        ),
        'public' => false,
        'show_ui' => true,
        'supports' => array('title', 'thumbnail'),
        'has_archive' => false,
        'menu_icon' => 'dashicons-carrot',
        'capability_type' => 'product',
        'map_meta_cap' => true
    ));

    // Products Taxonomy
    register_taxonomy('product-choices', 'products', array(
        'labels' => array(
            'name'          => __('Extra keuzes', 'menukaart'), 
            'singular_name' => __('Extra keuze', 'menukaart'),
            'add_new'       => __('Keuze aanmaken', 'menukaart'),
            'add_new_item'  => __('Nieuwe keuze maken', 'menukaart'),
            'new_item'      => __('Nieuwe keuze', 'menukaart'),
            'edit_item'     => __('Wijzig keuze', 'menukaart'),
            'view_item'     => __('Bekijk keuze', 'menukaart'),
            'all_items'     => __('Alle keuzes', 'menukaart'),
            'search_items'  => __('Zoek keuzes', 'menukaart'),
        ),
        'hierarchical'          => false,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'rewrite'               => false,
        'public'                => false,
        'meta_box_cb'           => false,
        'capabilities' => array(
            'manage_terms' => 'manage_product-choices',
            'edit_terms' => 'edit_product-choices',
            'delete_terms' => 'delete_product-choices',
            'assign_terms' => 'assign_product-choices',
        )
    ));

    if(get_option('printnode_api')) {

        // Tables
        register_post_type('tables', array(
            'labels' => array(
                'name'          => __('Tafels', 'menukaart'), 
                'singular_name' => __('Tafel', 'menukaart'),
                'add_new'       => __('Tafel aanmaken', 'menukaart'),
                'add_new_item'  => __('Nieuwe tafel maken', 'menukaart'),
                'new_item'      => __('Nieuwe tafel', 'menukaart'),
                'edit_item'     => __('Wijzig tafel', 'menukaart'),
                'view_item'     => __('Bekijk tafel', 'menukaart'),
                'all_items'     => __('Alle tafels', 'menukaart'),
                'search_items'  => __('Zoek tafels', 'menukaart'),
            ),
            'public' => false,
            'show_ui' => true,
            'supports' => array('title'),
            'has_archive' => false,
            'menu_icon' => 'dashicons-image-filter',
            'capability_type' => 'table',
            'map_meta_cap' => true
        ));
    }
}
add_action('init', 'create_posttype');

// Unregister default taxonomies
/*add_action('init', function(){
    global $wp_taxonomies;

    unregister_taxonomy_for_object_type('category', 'post');
    unregister_taxonomy_for_object_type('post_tag', 'post');

    if (taxonomy_exists('category')) unset($wp_taxonomies['category']);
    if (taxonomy_exists('translation_priority')) unset($wp_taxonomies['translation_priority']);
    if (taxonomy_exists('post_tag')) unset($wp_taxonomies['post_tag']);

    unregister_taxonomy('category');
    unregister_taxonomy('post_tag');
});*/

// Load posttype Tables options
require get_stylesheet_directory() . '/inc/posttype/tables.php';

// Load posttype Products options
require get_stylesheet_directory() . '/inc/posttype/products.php';

// Load posttype Menus options
require get_stylesheet_directory() . '/inc/posttype/menus.php';