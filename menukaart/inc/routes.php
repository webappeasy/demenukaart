<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Disable some default routes
add_filter('rest_endpoints', function($endpoints) {
    // Users endpoint
    if(isset($endpoints['/wp/v2/users'])) {
        unset($endpoints['/wp/v2/users']);
    }

    // Pages endpoint
    if(isset($endpoints['/wp/v2/pages'])) {
        unset($endpoints['/wp/v2/pages']);
    }

    // Posts endpoint
    if(isset($endpoints['/wp/v2/posts'])) {
        unset($endpoints['/wp/v2/posts']);
    }
    
    // Media endpoint
    if(isset($endpoints['/wp/v2/media'])) {
        unset($endpoints['/wp/v2/media']);
    }

    return $endpoints;
});

// Load products API
require get_stylesheet_directory() . '/inc/routes/products.php';