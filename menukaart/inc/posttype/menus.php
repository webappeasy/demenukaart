<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Add 'Current Menu' link to overview
add_filter('post_row_actions', 'add_menu_action', 10, 2);

function add_menu_action($actions, $page_object) {
    if($page_object->post_type == 'menu' && $page_object->ID != get_option('current_menu')) {
	    $actions['active-menu'] = '<a href="post.php?action=menu&post=' . $page_object->ID . '&_wpnonce=' . wp_create_nonce('menu-' . $page_object->ID) . '" class="menu_link">' . __('Stel in als huidig menu', 'menukaart') . '</a>';
    }

	return $actions;
}

// Handle action to set as current menu
add_action('load-post.php', 'set_menu_action', 10, 0);

function set_menu_action() {
	if(isset($_GET['action']) && $_GET['action'] == 'menu') {
		if(isset($_GET['post']) && isset($_GET['_wpnonce'])) {
			// Verify Nonce
			if(wp_verify_nonce($_GET['_wpnonce'], 'menu-' . $_GET['post'])) {
				if(get_option('current_menu')) {
					// Update if options exists
					update_option('current_menu', $_GET['post']);
					wp_redirect(admin_url('edit.php?post_type=menu'));
					exit;
				} else {
					// Create option if doesn't exists
					add_option('current_menu', $_GET['post']);
					wp_redirect(admin_url('edit.php?post_type=menu'));
					exit;
				}
			}
		}
	}
}

// Add 'Current Menu' state
add_filter('display_post_states', 'add_menu_state', 10, 2);

function add_menu_state($media_states, $post) {
	// Media set as magazine
	if($post->ID == get_option('current_menu')) {
		$media_states[] = __('Huidige menukaart', 'menukaart');
	}

	return $media_states;
}