<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Add QR-Code metabox to the WP Admin edit screen
function tables_meta_boxes() {
    add_meta_box('qr-code', __('QR-Code tafel', 'menukaart'), 'qr_callback', 'tables', 'side');
}
add_action('add_meta_boxes', 'tables_meta_boxes');

function qr_callback($post) {
    if($post->post_status == 'publish') {
        echo '<center><img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=' . urlencode(home_url('/?t=' . $post->ID)) . '" title="' . __('Download QR-Code voor deze tafel', 'menukaart') . '" /></center>';
        echo '<center><a href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=' . urlencode(home_url('/?t=' . $post->ID)) . '" target="_blank">' . __('Hogere resolutie', 'menukaart') . '</a></center>';
    } else {
        _e('Na het publiceren van de tafel verschijnt hier een QR-Code.', 'menukaart');
    }
}