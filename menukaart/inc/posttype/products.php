<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Add custom columns products overview
add_filter('manage_products_posts_columns', 'set_products_columns');

function set_products_columns($columns) {
    // New columns
    $columns['prices'] = __('Verkoopprijzen', 'menukaart');
    $columns['purchase-prices'] = __('Inkoopprijzen', 'menukaart');

    // Remove columns
    unset($columns['taxonomy-product-choices']);

    // Change order
    $n_columns = array();
    $move1 = 'prices'; // what to move
    $move2 = 'purchase-prices'; // what to move
    $before = 'date'; // move before this
    
    foreach($columns as $key => $value) {
        if($key == $before) {
            $n_columns[$move1] = $move1;
            $n_columns[$move2] = $move2;
        }

        $n_columns[$key] = $value;
    }

    return $n_columns;
}

// Add data to the custom columns on the products overview
add_action('manage_products_posts_custom_column' , 'custom_products_column', 10, 2);

function custom_products_column($column, $post_id) {
    switch ($column) {
    
        case 'prices' :
            if(get_field('product_units')) {
                foreach(get_field('product_units') as $price) {
                    echo $price['unit'] . ' € ' . number_format($price['price'], 2, ',', '.') . '<br>';
                }
            }
        break;

        case 'purchase-prices' :
            if(get_field('product_units')) {
                foreach(get_field('product_units') as $price) {
                    if($price['purchase_price']) {
                        echo $price['unit'] . ' € ' . number_format($price['purchase_price'], 2, ',', '.') . '<br>';
                    }
                }
            }
        break;
    
    }
}

// Add custom columns products taxonomy overview
add_filter('manage_edit-product-choices_columns', 'set_products_taxonomy_columns');

function set_products_taxonomy_columns($columns) {
    // New columns
    $columns['price'] = __('Verkoopprijs', 'menukaart');
    $columns['purchase-price'] = __('Inkoopprijs', 'menukaart');

    // Remove columns
    unset($columns['description']);
    unset($columns['slug']);

    // Change order
    $n_columns = array();
    $move1 = 'price'; // what to move
    $move2 = 'purchase-price'; // what to move
    $before = 'posts'; // move before this
    
    foreach($columns as $key => $value) {
        if($key == $before) {
            $n_columns[$move1] = $move1;
            $n_columns[$move2] = $move2;
        }

        $n_columns[$key] = $value;
    }

    return $n_columns;
}

// Add data to the custom columns on the taxonomy overview
add_action('manage_product-choices_custom_column' , 'custom_products_taxonomy_column', 10, 3);

function custom_products_taxonomy_column($string, $column, $id) {
    switch ($column) {
    
        case 'price' :
            if(get_field('price', 'product-choices_' . $id)) {
                echo '€ ' . number_format(get_field('price', 'product-choices_' . $id), 2, ',', '.');
            }
        break;

        case 'purchase-price' :
            if(get_field('purchase_price', 'product-choices_' . $id)) {
                echo '€ ' . number_format(get_field('purchase_price', 'product-choices_' . $id), 2, ',', '.');
            }
        break;
    
    }
}