<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Load scripts front-end
function enqueue_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap-js', get_theme_file_uri('/assets/bootstrap/js/bootstrap.min.js'), array(), false, true);
    wp_enqueue_script('instascan-js', get_theme_file_uri('/assets/jsqr/jsQR.js'), array(), false, true);
    wp_enqueue_script('theme-js', get_theme_file_uri('/js/theme.min.js'), array(), time(), true);

    // Localized scripts
    wp_localize_script('theme-js', 'menuData', array(
        'root' => esc_url_raw(home_url('wp-json/')),
        'nonce' => wp_create_nonce('wp_rest'),
        'lang' => apply_filters('wpml_current_language', NULL),
        'addItem' => __('Toevoegen aan bestelling', 'menukaart'),
        'orderNow' => __('Nu bestellen', 'menukaart'),
        'subTotal' => __('Subtotaal', 'menukaart')
    ));
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');

// Load scripts admin
function enqueue_admin_scripts($hook) {
    wp_enqueue_script('jquery');
    wp_enqueue_script('tabulator-js', get_theme_file_uri('/assets/tabulator/js/tabulator.min.js'), array(), false, true);
    wp_enqueue_script('xlsx-js', get_theme_file_uri('/assets/tabulator/js/xlsx.full.min.js'), array(), false, true);
    wp_enqueue_script('jspdf-js', get_theme_file_uri('/assets/tabulator/js/jspdf.min.js'), array(), false, true);
    wp_enqueue_script('autotable-js', get_theme_file_uri('/assets/tabulator/js/jspdf.plugin.autotable.js'), array(), false, true);
    wp_enqueue_script('admin-js', get_theme_file_uri('/js/admin.min.js'), array(), time(), true);
}
add_action('admin_enqueue_scripts', 'enqueue_admin_scripts');